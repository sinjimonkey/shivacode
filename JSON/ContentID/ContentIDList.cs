﻿using ShivaCode.Events;
using ShivaCode.JSON;
using UnityEngine;

namespace ShivaCode.Network
{
    [CreateAssetMenu(menuName = "Prism/Data/ContentIDList", fileName = "New ContentID List")]
    public class ContentIDList : FileAutoReference
    {

        
        public void SetKeys(string keyPath, JSONObject values)
        {
            
            Log($"SetKeys:  {keyPath} -> {values}");
            foreach (var v in values.keys)
            {
                var field = values.GetField(v);
                if (field.HasField("id"))
                    SetString(keyPath + "/" + v, field.ToString());
            }

            JSON = RawString;
            if (autoSave)
                Save();
        }

    }
    
    
}
