﻿using Vuforia;

namespace ShivaCode.Network
{
    public struct ContentID
    {
        public int ID;
        public string Reference;

        public bool IsInternal => Reference.ToLower().Contains("internal");
        
        public ContentID(string reference, int id)
        {
            this.Reference = reference;
            this.ID = id;
        }

        public string FullPath =>   IsInternal ? Reference : Reference+"."+ID;
        public string SavePath =>   IsInternal ? Reference : DeviceWrapper.DeviceWrapper.SanitizePath(Reference+"."+ID);

        public override string ToString()
        {
            return $"Content ID {{ id:  {ID}, ref:  {Reference} }}".ToLower();
        }
    }
}