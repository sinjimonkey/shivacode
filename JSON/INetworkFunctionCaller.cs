﻿using System;

namespace ShivaCode.Network
{
    public interface INetworkFunctionCaller
    {
        void NetworkFunctionCall(string function, object args, bool generateEvent, Action<JSONObject> resultHandler,
            Action<string> errorHandler);
    }
}