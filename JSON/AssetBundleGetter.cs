﻿using System;
using System.Collections.Generic;
using ShivaCode.Events;
using ShivaCode.Interfaces;
using ShivaCode.ProcessWedges.NetworkWedges;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ShivaCode.Network
{
    public class AssetBundleGetter : BaseMonobehaviour, IConverter<ContentID, AssetBundle>,
        IConverter<ContentID, UnityEngine.Object[]>
    {
        private static Dictionary<string, AssetBundle> bundles = new Dictionary<string, AssetBundle>();

        private static Dictionary<string, UnityEngine.Object[]> extracted =
            new Dictionary<string, UnityEngine.Object[]>();

        [SerializeField] protected INetworkGetBundle localBundles;
        [SerializeField] protected INetworkGetBundle networkBundles;
        [SerializeField] public INetworkGetBundle addressableBundles;

        public static bool CheckBundle(ContentID contentId, out AssetBundle assetBundle)
        {
            AssetBundle output = null;
            bool check = CheckBundle(contentId, (x) => { output = x; });
            assetBundle = output;
            return check;
        }

        public static bool CheckExtracted(ContentID contentId, out Object[] objectArray)
        {
            Object[] output = null;
            bool check = CheckExtracted(contentId, (x) => { output = x; });
            objectArray = output;
            return check;
        }

        public static void StoreBundle(ContentID contentId, AssetBundle assetBundle)
        {
            if (!bundles.ContainsKey((contentId.Reference)))
                bundles.Add(contentId.Reference, assetBundle);
            else
                bundles[contentId.Reference] = assetBundle;
        }

        public static void StoreExtracted(ContentID contentId, Object[] objectArray)
        {
            if (!extracted.ContainsKey((contentId.Reference)))
                extracted.Add(contentId.Reference, objectArray);
            else
                extracted[contentId.Reference] = objectArray;
        }

        private static bool CheckBundle(ContentID input, Action<AssetBundle> output)
        {
            if (!bundles.ContainsKey(input.Reference) || bundles[input.Reference] == null)
                return false;
            output.Invoke(bundles[input.Reference]);
            return true;
        }

        private static bool CheckExtracted(ContentID input, Action<UnityEngine.Object[]> output)
        {
            if (!extracted.ContainsKey(input.Reference) || extracted[input.Reference] == null ||
                extracted[input.Reference].Length <= 0) return false;
            output.Invoke(extracted[input.Reference]);
            return true;
        }

        public static void Unload(ContentID content, bool unloadAll = true)
        {
            var key = content.Reference;
            if (unloadAll && extracted.ContainsKey(key))
                extracted.Remove(key);
            if (bundles.ContainsKey(key) && bundles[key] != null)
                bundles[key].Unload(unloadAll);
        }

        public void ConvertContentIDToAssetBundleExternal(ContentID input, GUIDEventObject GUID)
        {
            Convert(input, GUIDEvent.GUIDWrap<AssetBundle>(GUID, true), GUIDEvent.GUIDWrap<string>(GUID, ErrorMessage));
        }

        public void Convert(ContentID input, Action<AssetBundle> output, Action<string> error = null)
        {
            if (CheckBundle(input, output))
                return;

            baseDone = false;

            // wrap the output to add a key to the dictionary.
            var wrap = new Action<AssetBundle>(bundle =>
            {
                if (CheckBundle(input, output)) // if it is already set by another process we skip.
                    return;
                if (bundle != null)
                    bundles.Add(input.FullPath, bundle);
                output(bundle);
            });

            var s = input.FullPath.ToLower();

            if (addressableBundles != null && s.Contains("internal"))
                TryAddress(input, wrap, error);
            else if (localBundles != null)
                TryLoad(input, wrap, error, true);
            else
                TryNetworkLoad(input, wrap, error);
        }

        private void TryAddress(ContentID input, Action<AssetBundle> output, Action<string> error)
        {
            UnityEngine.Debug.Log("TryAddress:  " + input.FullPath);

            void Local(AssetBundle bundle)
            {
                if (bundle != null)
                    output.Invoke(bundle);
                else if (localBundles != null)
                    TryLoad(input, output, error, true);
                else if (networkBundles != null)
                    TryNetworkLoad(input, output, error);
                else
                {
                    error?.Invoke("Get Bundle Failed:  AssetBundleGetter.TryAddress");
                    output(null);
                }
            }

            addressableBundles?.GetBundle(input.FullPath, Local, error);
        }



        private void TryLoad(ContentID input, Action<AssetBundle> output, Action<string> error, bool recurse = false)
        {
            UnityEngine.Debug.Log($"TryLoad:  input.FullPath  ( recurse = {recurse} )");

            void Local(AssetBundle bundle)
            {
                UnityEngine.Debug.Log($"     Local:  ( {bundle} - {recurse} )");

                if (bundle != null)
                    output.Invoke(bundle);
                else if (recurse)
                    TryNetworkDownload(input, output, error);
                else
                {
                    error?.Invoke("AssetBundle Get Failed:  AssetBundleGetter.TryLoad");
                    output.Invoke(null);
                }
            }

            if (localBundles == null)
                LogWarning("LocalBundles not set in " + this.name);
            else
                localBundles?.GetBundle(input.FullPath, Local, x =>
                {
                    Local(null);
                    error?.Invoke(x);
                });
        }


        private void TryNetworkLoad(ContentID input, Action<AssetBundle> output, Action<string> error)
        {
            //does not save - use this as a last resort if we can't save.
            if (networkBundles == null)
                output.Invoke(null);
            else
                networkBundles.GetBundle(input.FullPath, output, error);
        }

        public void TryAddressableLoadExternal(ContentID input, GUIDEventObject GUID)
        {

            //Debug.Log($"{contentGetter}");
            //Debug.Log($"{contentGetter.addressableBundles}");
            //Debug.Log($"{id}");
            //Debug.Log($"{id.FullPath}");
            addressableBundles.GetBundle(input.FullPath.ToLower(), GUIDEvent.GUIDWrap<AssetBundle>(GUID, true),
                GUIDEvent.GUIDWrap<object>(GUID));
        }
    

    public void TryNetworkDownloadExternal(ContentID input, GUIDEventObject GUID)
        {
            networkBundles.SaveBundle(input.FullPath, GUIDEvent.GUIDWrap<bool>(GUID, true), GUIDEvent.GUIDWrap<object>(GUID));
        }
        public void TryLoadExternal(ContentID input, GUIDEventObject GUID)
        {
            localBundles.GetBundle(input.SavePath, GUIDEvent.GUIDWrap<AssetBundle>(GUID, true), GUIDEvent.GUIDWrap<object>(GUID));
        }

        private void TryNetworkDownload(ContentID input, Action<AssetBundle> output, Action<string> error) {
            UnityEngine.Debug.Log("TryNetwork:  " + input.FullPath);
            void Network(bool success)
            {
                UnityEngine.Debug.Log("Try Network Download Success?  "+success);
                if (success)
                    TryLoad(input, output, error, false);
                else
                {
                    error?.Invoke("Saving File Failed in AssetBundleGetter.TryNetworkDownload");
                    output.Invoke(null); // we didn't succeed in saving the file.
                }
            }

            if (networkBundles == null) {
                error?.Invoke("NetworkBundles not set in " + this.name);
                output.Invoke(null);
            } else
                networkBundles.SaveBundle(input.FullPath, Network, error);
        }

        public void Convert(ContentID input, Action<Object[]> output, Action<string> error = null)
        {
            Convert(input, output, error, false);
        }

        public void Convert(ContentID input, Action<Object[]> output, Action<string> error, bool recurse = false)
        {

            var s = input.FullPath.ToString();
            if (recurse && addressableBundles != null && s.Contains("internal")){
                void getReturn(Object[] goa) {
                    if (goa != null && goa.Length > 0 && goa[0] != null)
                        output(goa);
                    else
                        Convert(input, output, error, false);
                    
                }
                addressableBundles.GetObjects(s, getReturn, x => { });
                return;
            }

            if (CheckExtracted(input, output))
                return;
            
            void ExtractBundle(AssetBundle bundle)
            {
                UnityEngine.Debug.Log("Bundle for Extraction:  " + bundle);
                if (CheckExtracted(input, output)) // because another process might have done this already.
                    return;
                BundleExtractor.Extract(bundle, (x) =>
                {
                    if (extracted.ContainsKey(input.Reference)) {
                        //extracted.Remove(input.Reference);
                        output(extracted[input.Reference]);
                        return;
                    }
                    extracted.Add(input.Reference, x);


                    output(x);
                });
            }
            
            Convert(input, ExtractBundle, error);
        }

        private void ErrorMessage(string x)
        {
            UnityEngine.Debug.LogError(x);
        }
    }
}