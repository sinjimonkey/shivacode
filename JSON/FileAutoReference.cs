﻿using System;
using System.Collections.Generic;
using ShivaCode.Events;
using ShivaCode.Interfaces;
using ShivaCode.Network;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;
using Debug = UnityEngine.Debug;

namespace ShivaCode.JSON
{
    [CreateAssetMenu(fileName = "New FileReference", menuName = "ShivaCode/ScriptableData/File Reference")]
    public class FileAutoReference : ShivaJSON, IReturnHandler<string>
    {
        public static Queue<FileAutoReference> DirtyList = new Queue<FileAutoReference>();
        
            
        [Serializable]
        public enum ReferenceType
        {
            PlayerData,
            TitleData,
            LocalData
        }

        [PropertyOrder(-1)] public string path;
        [PropertyOrder(-1)] public ReferenceType referenceType;
        [PropertyOrder(-1), ShowIf("IsTitle")] public INetworkGetTitleData titleServer = null;
        [PropertyOrder(-1), ShowIf("IsPlayer")] public INetworkGetPlayerData playerServer = null;
        [PropertyOrder(-1)] public bool allowDeviceBackup = false;
        [PropertyOrder(-1)] public bool autoSave = true;

        private bool _dirty = false;
        
        private bool IsTitle => referenceType == ReferenceType.TitleData;
        private bool IsPlayer => referenceType == ReferenceType.PlayerData;


        public override void SetString(string key, string val)
        {
            base.SetString(key, val);
            if (autoSave)
                Save();
            else
                FlagDirty();
        }

        private void FlagDirty()
        {
            _dirty = true;
            if (!DirtyList.Contains(this))
               DirtyList.Enqueue(this);
        }

        [Button]
        public virtual void Load(GUIDEventObject GUID = default)
        {
            Action<string> resultHandler = GUIDEvent.GUIDWrap<string>(GUID, Handle, true);
            Action<string> errorHandler = GUIDEvent.GUIDWrap<string>(GUID, LogError, false);
            
            UnityEngine.Debug.Log("Start Load:  "+path);
            if (IsTitle)
                titleServer?.GetNetworkTitleKey(path, resultHandler, errorHandler);
            if (IsPlayer)
                playerServer?.GetNetworkPlayerKey(path, resultHandler, errorHandler);
        }

        public virtual void LoadWithTitleDataServer(INetworkGetTitleData titleDataServer)
        {
            if (!IsTitle)
                LogError($"{name} attempted to load Title with Title Data Server");
            titleDataServer.GetNetworkTitleKey(path, Handle, LogError);
        }
        
        public virtual void LoadWithPlayerDataServer(INetworkGetPlayerData playerDataServer)
        {
            if (!IsTitle)
                LogError($"{name} attempted to load Title with Player Data Server");
            playerDataServer.GetNetworkPlayerKey(path, Handle, LogError);
        }

        [Button]
        public virtual void Save()
        {
            Log($"Save:  {path}\n\n---------------------\n{RawString}");
            if (IsTitle)
                titleServer?.SetNetworkTitleKey(path, RawString, !allowDeviceBackup);
            if (IsPlayer)
                playerServer?.SetNetworkPlayerKey(path, RawString, !allowDeviceBackup);
            _dirty = false;
        }

        /*protected override void Initialize()
        {
            base.Initialize();
            Load();
        }*/
        public virtual void Handle(string result)
        {
            if (!result.Contains("{")) result = "{" + result;
            if (!result.Contains("}")) result = result + "}";
            UnityEngine.Debug.Log(name+" Handle Loaded:  "+result);
            RawString = result;
            JSON = RawString;
            baseDone = true;
        }

    }
}
