﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;
using ShivaCode.Math;
//using PlayFab.Json;
using Sirenix.OdinInspector;
using UnityEngine;
using Debug = UnityEngine.Debug;

//using static PlayFab.Json.PlayFabSimpleJson;

namespace ShivaCode.JSON
{
    public class ShivaJSON : BaseScriptable
    {
        
        public string FixQuotes(string original)
        {
            var unescapeQuotes = original.Replace("\\\"","\"");
            var split = unescapeQuotes.Split('"');
            if (split.Length == 1)
                return original;

            var ret = "";

            for (int i = 0; i < split.Length; i++)
            {
                if (i % 2 == 0)
                    ret += split[i];
                else
                    ret += $"“{split[i]}”";
            }
            
            return ret;
        }
        
        private struct SubKey
        {
            public string Key;
            public string Path;
            public SubKey(string key)
            {
                var split = key.Split('/');
                var list = split.ToList();
                Key = split[0];
                list.RemoveAt(0);
                split = list.ToArray();
                Path = string.Join("/",split);
            }
        }

        public static ShivaJSON Instantiate(JSONObject x) {
            return Instantiate(x.ToString());
        }

        public static ShivaJSON Instantiate(string s) {
            var ret = CreateInstance<ShivaJSON>();
            ret.RawString = s;
            return ret;
        }

        [SerializeField, HideInInlineEditors] private JSONObject data = new JSONObject();

        public string RawString
        {
            get => data.ToString(true).Replace("\t","    ");
            set => data = new JSONObject(value);
        }


        public virtual string GetString(string key, string def = "", bool escape = false)
        {
            var json = data;
            while (key.Contains('/'))
            {
                var sub = new SubKey(key);
                key = sub.Path;
                json = json.GetField(sub.Key);
                if (json == null)
                    return def;
            }

            JSONObject ret = json.GetField(key);
            if (ret != null) {
                var s = ret.ToString();

                s = s.Trim();
                while (s.Length > 0 && ( s[0].Equals('"') || s[0].Equals("'") ))
                    s = s.Substring(1);

                while (s.Length > 0 &&  ( s[s.Length-1].Equals('"') || s[s.Length-1].Equals("'") ))
                    s = s.Substring(0,s.Length-1);

                s = s.Replace("//", "/");
                s = s.Replace("//n", "/n");
                s = s.Replace("//t", "/t");
                s = s.Replace("//r", "/r");

                return s;
            }

            if (escape)
            {
                string quote = @"\" + '"';
                if (def.Contains(key))
                    def.Replace(key, "\"");
                def.Replace("\"", quote);
            }

            return def;
        }

        public virtual void SetString(string key, string val)
        {
            if (data == null) data = new JSONObject();
            var json = data;
            
            while (key.Contains('/'))
            {
                var sub = new SubKey(key);
                key = sub.Path;
                var temp = json.GetField(sub.Key);
                if (temp == null)
                {
                    temp = new JSONObject();
                    json.AddField(sub.Key, temp);
                }

                json = temp;
            }

            val = FixQuotes(val);
            json.SetField(key, val);
        }

        public bool GetBool(string key, bool def = false)
        {
            var x = GetString(key, def + "");
            var n = Boolean.TryParse(x, out var y);
            if (n) return y;
            var i = GetInt(key, -1);
            switch (i)
            {
                case 0:
                    return false;
                case 1:
                    return true;
                default:
                    return def;
            }
        }

        public void SetBool(string key, bool val)
        {
            SetString(key, val+"");
        }
        
        public int GetInt(string key, int def = 0)
        {
            var x = GetString(key, def + "");
            var n = int.TryParse(x, out var y);
            return n ? y : def;
        }

        public void SetInt(string key, int val)
        {
            //Debug.Log("BreakPoint");
            if (val != 0)
                SetString(key, val+"");
            else
                SetString(key, val+"");
        }
        
        public float GetFloat(string key, float def = 0)
        {
            var x = GetString(key, def + "");
            var n = float.TryParse(x, out var y);
            return n ? y : def;
        }

        public void SetFloat(string key, float val)
        {
            SetString(key, val+"");
        }
        
        public double GetDouble(string key, double def = 0)
        {
            var x = GetString(key, def + "");
            var n = float.TryParse(x, out var y);
            return n ? y : def;
        }

        public void SetDouble(string key, double val)
        {
            SetString(key, val+"");
        }

        public Vector3 GetVector3(string key, Vector3 def) {
            var x = GetString(key, $"{def.x},{def.y},{def.z}");
            var sa = x.Split(',');

            if (!float.TryParse(sa[0], out float sx))
                sx = def.x;
            if (sa.Length < 2 || !float.TryParse(sa[1], out float sy))
                sy = def.y;
            if (sa.Length < 3 || !float.TryParse(sa[2], out float sz))
                sz = def.z;

            return new Vector3(sx, sy, sz);
        }

        public void SetVector3(string key, Vector3 val) {
            SetString(key, $"{val.x},{val.y},{val.z}");
        }

        public Vector2 GetVector2(string key, Vector2 def) {
            var x = GetString(key, $"{def.x},{def.y}");
            var sa = x.Split(',');
            float sx, sy;
            if (!float.TryParse(sa[0], out sx))
                sx = def.x;
            if (sa.Length < 2 || !float.TryParse(sa[1], out sy))
                sy = def.y;

            return new Vector2(sx, sy);
        }

        public void SetVector2(string key, Vector2 val) {
            SetString(key, $"{val.x},{val.y}");
        }

        public String[] GetList(string key, char delim = '|')
        {
            var str = GetString(key,"");
            if (String.IsNullOrWhiteSpace(str))
                return new String[]{};
            var ret = str.Split(delim);
            
            return ret;
        }
// test chage
        public void SetList(string key, String[] value, char delim = '|')  //
        {  //
            var str = "";  //
            if (value.Length == 1)  //
                str = value[0];  //
            else if (value.Length > 1)  //
                str = String.Join(delim.ToString( ), value);  //
            SetString(key, str); //
        } //

        public DoubleVector GetDoubleVector(string key)
        {
            return DoubleVector.ParseGPS(GetString(key,""));
        }


        public void SetDoubleVector(string key, DoubleVector value)
        {
            SetString(key,value.lat+", "+value.lng);
        }


        // ReSharper disable once InconsistentNaming
        [ShowInInspector, PropertyOrder(8500), TextArea(5,10),HideInInlineEditors] public string JSON;

        [Button, PropertyOrder(8499)]
        public virtual void DisplayJSON()
        {
            JSON = RawString;
        }

        private new void OnValidate()
        {
            JSON = RawString;
            base.OnValidate();
        }
    }
}
