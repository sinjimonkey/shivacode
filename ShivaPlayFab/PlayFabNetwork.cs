﻿#define PLAYFAB
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ShivaCode;
using ShivaCode.DeviceWrapper;
using ShivaCode.Events;
using ShivaCode.Network;
using ShivaCode.ProcessWedges;
using ShivaCode.ScriptableData;
using UnityEngine;
using UnityEngine.Networking;
using ShivaCode.DeviceWrapper;
using ProgressTracker = ShivaCode.ProcessWedges.ProgressTracker;
#if PLAYFAB
using PlayFab;
using PlayFab.ClientModels;
#endif

namespace Plugins.ShivaCode.PlayFab
{
    
    [Serializable]
    [CreateAssetMenu(fileName = "PlayFab Network", menuName = "ShivaCode/Network/PlayFab/PlayFab")]
    public class PlayFabNetwork : BaseScriptable, INetworkGetTitleData, INetworkGetPlayerData, INetworkGetMOTD, INetworkFunctionCaller, INetworkGetBundle
    {
        public LocalDiskIO localBundleBackup;
        public BoolData isOnline;
        public BoolData isConnectedToPlayFab;
        public BoolData loginFailed;
        public BoolData doUseOffline;
        public bool autoLogin = true;

        private bool loggingIn = false;
        

        // ReSharper disable once MemberCanBePrivate.Global
        private void DoIfLoggedIn(Action act)
        {
            CheckPlayFab();
            if (isConnectedToPlayFab == null)
            {
                LogWarning("No Boolean 'isConnectedToPlayFab' set");
                return;
            }

            if (isConnectedToPlayFab.boolData)
            {
                act.Invoke();
            }
            else if (autoLogin)
            {
                Actions.Enqueue(act);
                AutoLogIn();
            }
        }

        // ReSharper disable once MemberCanBePrivate.Global
        private void AutoLogIn()
        {
            Log("Attempting Login");
            DeviceWrapper.DoWithDevice((x) =>LogInByDevice(x));   
        }

        public void BoltLogIn(GUIDEventObject GUID)
        {
            Log("Attempting Login via Bolt");
            DeviceWrapper.DoWithDevice((x)=>LogInByDevice(x, GUID));   
        }


        private void LogInByDevice(DeviceWrapper device, GUIDEventObject GUID = default)
        {
            loggingIn = true;
            Log($"Loggin by device - {device.DeviceKey}");
#if PLAYFAB
            Log($"Logging into PlayFab - {device.DeviceKey}");
            Action<Action<LoginResult>,Action<PlayFabError>> logInAction = (x,y) => { } ;
            
            switch (device.DeviceKey)
            {
                case "Android":
                    var androidRequest = new LoginWithAndroidDeviceIDRequest{AndroidDeviceId = SystemInfo.deviceUniqueIdentifier, CreateAccount = true };
                    logInAction = (x, y) => {
                        PlayFabClientAPI.LoginWithAndroidDeviceID(androidRequest, x, y);
                        Log("Android Log in!");
                    };
                    break;
                case "iOS":
                    var iosRequest = new LoginWithIOSDeviceIDRequest{DeviceId = SystemInfo.deviceUniqueIdentifier, CreateAccount = true };
                    logInAction = (x, y) => {
                        PlayFabClientAPI.LoginWithIOSDeviceID(iosRequest, x, y);
                        Log("iOS Log in!");
                    };
                    break;
            }
            Log("LoginAction:  " + device.DeviceKey);
            
            logInAction.Invoke(
                GUIDEvent.GUIDWrap<LoginResult>(GUID, LoginSuccess, true), 
                GUIDEvent.GUIDWrap<PlayFabError>(GUID, LoginFail, false)
                );
#endif

        }
#if PLAYFAB
        private void LoginSuccess(LoginResult result)
        {
            StartQueue();
            isConnectedToPlayFab.SetTrue();
            Log("Login Success");
            loggingIn = false;
        }

        private void LoginFail(PlayFabError error) {
            Log("Login Failure");
            LogError(error.ErrorMessage);
            loggingIn = false;
            loginFailed.SetTrue();
        }
#endif

        private bool CheckPlayFab()
        {
#if PLAYFAB
            return true;
#else
            LogError("Cannot use PlayFab Network - please make sure to #define PLAYFAB if you have the PlayFab SDK Installed");
            return false;
#endif
        }
        
        private static readonly Queue<Action> Actions = new Queue<Action>();
        private static bool throttling = false;
        // ReSharper disable once FieldCanBeMadeReadOnly.Global
        // ReSharper disable once ConvertToConstant.Global
        // ReSharper disable once MemberCanBePrivate.Global
        public static float ThrottleDelay = 0.1f;
        private INetworkGetMOTD _networkGetMOTDImplementation;

        public static void ThrottleAction(Action act)
        {
            Actions.Enqueue(act);
            if (!throttling)
            {
                StartQueue();
            }

        }

        public static void StartQueue()
        {
            CoroutineStarter.AddCoroutine(RunQueue());
        }

        private static IEnumerator RunQueue()
        {
            throttling = true;
            while (Actions.Count > 0)
            {
                RunNext();
                yield return new WaitForSeconds(ThrottleDelay);
            }

            throttling = false;
        }

        private static void RunNext()
        {
            var act = Actions.Dequeue();
            act.Invoke();
        }

        
        public void GetNetworkTitleKeyExternal(string path, GUIDEventObject GUID) //
        { //
            GetNetworkTitleKey(path, GUIDEvent.GUIDWrap<string>(GUID, true), GUIDEvent.GUIDWrap<string>(GUID)); //
        } //

        public void GetNetworkPlayerKeyExternal(string path, GUIDEventObject GUID) //
        { //
            GetNetworkPlayerKey(path, GUIDEvent.GUIDWrap<string>(GUID, true), GUIDEvent.GUIDWrap<string>(GUID)); //
        } //


        public void GetNetworkTitleKey(string path, Action<string> callback, Action<string> errorCallback)
        {
            void act(Dictionary<string, string> dict)
            {
                if (dict.ContainsKey(path))
                    callback(dict[path]);
                else
                {
                    UnityEngine.Debug.LogError("Key not found:  "+path);
                }
            }
            
            var list = new List<string>();
            list.Add(path);
            
            GetNetworkTitleKeys(list, act,  (x)=>UnityEngine.Debug.LogError(x));
        }

        public void SetNetworkTitleKey(string path, string text, bool noBackup = true)
        {
            throw new NotImplementedException();
        }

        public void GetNetworkTitleKeys(IEnumerable<string> paths, Action<Dictionary<string, string>> callback, Action<string> errorCallback)
        {
#if PLAYFAB
            UnityEngine.Debug.Log("PlayFab GetNetworkTitleKey Called");
            
            void ResCB(GetTitleDataResult result)
            {
                callback(result.Data);
            }

            void ErrorCB(PlayFabError error)
            {
                errorCallback(error.ErrorMessage);
            }

            var request = new GetTitleDataRequest {Keys = paths.ToList()};
            
            DoIfLoggedIn(() => PlayFabClientAPI.GetTitleData(request, ResCB, ErrorCB));
#endif
        }

        public void GetNetworkMOTD(Action<Dictionary<string, string>> callback, Action<string> errorCallback)
        {
            CheckPlayFab();
#if PLAYFAB
            UnityEngine.Debug.Log("PlayFab MOTD Called");
            void ResCB(GetTitleNewsResult result)
            {
                var ret = result.News.ToDictionary(v => v.Title, v => v.Body);
                UnityEngine.Debug.Log("PlayFab MOTD Returned");
                foreach (var key in  ret.Keys)
                {
                    UnityEngine.Debug.Log($"{key}:  {ret[key]}");
                }
                callback(ret);
            }

            void ErrorCB(PlayFabError error)
            {
                errorCallback(error.ErrorMessage);
            }
            
            DoIfLoggedIn(() => PlayFabClientAPI.GetTitleNews(new GetTitleNewsRequest(), ResCB, ErrorCB));
#endif
        }


        public void GetNetworkPlayerKey(string path, Action<string> callback, Action<string> errorCallback)
        {
            throw new NotImplementedException();
        }

        public void SetNetworkPlayerKey(string path, string text, bool noBackup = true)
        {
            throw new NotImplementedException();
        }

        public void GetNetworkPlayerKeys(IEnumerable<string> paths, Action<Dictionary<string, string>> callback, Action<string> errorCallback)
        {
            throw new NotImplementedException();
        }

        public void NetworkFunctionCall(string function, object args, bool generateEvent, Action<JSONObject> resultHandler, Action<string> errorHandler)
        {
            UnityEngine.Debug.LogWarning($"{function} = {args.ToString()}");
            void Act()
            {
                var request = new ExecuteCloudScriptRequest()
                {
                    FunctionName = function,
                    FunctionParameter = args,
                    GeneratePlayStreamEvent = generateEvent
                };
                if (Application.isEditor)
                {
                    request.GeneratePlayStreamEvent = true;
                }
                void PlayFabReturn(ExecuteCloudScriptResult requestResult)
                {
                    
                    if (requestResult.FunctionResult == null)
                        errorHandler("Null Return");
                    else
                    {
                        
                        var s = requestResult.FunctionResult.ToString();
                        //Debug.Log("PlayFab Return:  "+requestResult.FunctionName+" ->\n" + s);
                        var json = new JSONObject(s);
                        

                        if (!json.HasField("resultType"))
                            errorHandler("Invalid Return");
                        else if (!json.HasField("messageValue"))
                            errorHandler("Invalid Return");
                        else if (json["resultType"].str == "error")
                            errorHandler(json["messageValue"].ToString());
                        else
                           resultHandler(json);
                    }

                }

                void ErrorReturn(PlayFabError error)
                {
                    errorHandler(error.ErrorMessage);
                }

                PlayFabClientAPI.ExecuteCloudScript(request, PlayFabReturn, ErrorReturn);
            }

            DoIfLoggedIn(Act);
        }

        public void GetBundle(string path, Action<AssetBundle> callback, Action<string> errorCallback)
        {
            void URLHandler(string s)
            {
                CoroutineStarter.AddCoroutine(DownloadBundle(s, callback, errorCallback));
            }

            
            
            GetDownloadURL(path, URLHandler, errorCallback);
        }

        public void SaveBundle(string path, Action<bool> callback, Action<string> errorCallback)
        {
            UnityEngine.Debug.Log("Saving:  "+path);
            void Error(string s)
            {
                errorCallback(s);
                callback(false);
            }

            void URLHandler(string s)
            {
                UnityEngine.Debug.Log("Handle Download URL:  "+s);
                DeviceWrapper.DoWithDevice(device =>
                {
                    UnityEngine.Debug.Log($"Download Saved Bundle ({device}):  {s}");
                    CoroutineStarter.AddCoroutine(DownloadSavedBundle(device, path, s, callback, Error));
                });
            }
            
            GetDownloadURL(path, URLHandler, Error);
        }

        private void GetDownloadURL(string path, Action<string> callback, Action<string> errorCallback)
        {
            path = DeviceWrapper.PrependDevicePath(path);
            UnityEngine.Debug.Log("Getting Download URL:  "+path);
#if PLAYFAB

            DoIfLoggedIn(() =>
            {
                UnityEngine.Debug.Log("Network Content DownloadURL Request:  "+path);
                var request = new GetContentDownloadUrlRequest {Key = path};
                PlayFabClientAPI.GetContentDownloadUrl(request, (x) =>
                    {
                        UnityEngine.Debug.Log("Network Content Return:  "+x.URL);
                        callback.Invoke(x.URL);
                    },
                    (x) => errorCallback(x.ErrorMessage));
            });
#endif
        }
        //private bool _downloadLock = false;

        private static IEnumerator DownloadSavedBundle(DeviceWrapper device, string path, string url, Action<bool> callback,
            Action<string> errorCallback, bool noBackup=true)
        {
            path = DeviceWrapper.SanitizePath(path);
            path = DeviceWrapper.PrependDataPath(path, true);
            
            var handler = new DownloadHandlerFile(path) {removeFileOnAbort = true};
            UnityWebRequest request;
            try
            {
                device.CreateDirectories(path, noBackup);
                request = UnityWebRequestAssetBundle.GetAssetBundle(url);
                request.downloadHandler = handler;
                UnityEngine.Debug.Log("Sending Web Request:  "+url);
                var webAsync = request.SendWebRequest();

            }
            catch (Exception e)
            {
                errorCallback(e.Message);
                yield break;
            }
            
            ProgressTracker.BundleProgress.AddBit(new ProgressTracker.ProgressBit(()=>request.downloadProgress,200,"Download"));
            
            while (!handler.isDone)
            {
                var v = request.downloadProgress;
                UnityEngine.Debug.Log($"Download Progress:  {v}");
                if (v >= 1)
                    break;
                yield return null;
            }       
            device.SetNoBackup(path, noBackup);
            UnityEngine.Debug.Log("Download Finished - Bytes:  "+request.downloadedBytes);
            if (request.isNetworkError)
                errorCallback("Network Error:  " + request.error);
            else if (request.isHttpError)
                errorCallback("HTTP Error:  " + request.error);
            else
                callback(true);
        }


        private static IEnumerator DownloadBundle(string url, Action<AssetBundle> callback, Action<string> errorCallback)
        {
            //if (_downloadLock) yield break;
            //_downloadLock = true;
            var request = UnityWebRequestAssetBundle.GetAssetBundle(url, 0);
            var handler = new DownloadHandlerAssetBundle(url, 0);
            request.downloadHandler = handler;
            
            UnityEngine.Debug.Log("Sending URL request:  "+url);

            var process = request.SendWebRequest();
            while (!process.isDone)
            {
                yield return  new WaitForSeconds(0.1f);
                //Debug.Log("Download:  "+process.progress);
            }
            
            UnityEngine.Debug.Log("URL Request Returned");
            //_downloadLock = false;

            if (request.isNetworkError)
                errorCallback.Invoke(request.error);
            else 
                callback.Invoke(handler.assetBundle);
        }

        public void GetObjects(string path, Action<UnityEngine.Object[]> callback, Action<string> errorCallback) {
            IEnumerator asyncExtract(AssetBundle bundle) {
                var v = bundle.LoadAllAssetsAsync();
                yield return v;
                callback(v.allAssets);
            }

            void doBundle(AssetBundle bundle) {
                CoroutineStarter.AddCoroutine(asyncExtract(bundle));
            }

            GetBundle(path, doBundle, errorCallback);
        }
    }
}