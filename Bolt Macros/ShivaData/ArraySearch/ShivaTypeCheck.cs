﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace ShivaCode.Bolt
{


    public class ShivaTypeCheck
    {
        
        public static bool IsVideoClip(object obj)
        {
            if (obj == null) return false;
            try
            {
                var check = (VideoClip) obj;
                return check != null;
            }
            catch
            {
                return false;
            }

        }
    }
}