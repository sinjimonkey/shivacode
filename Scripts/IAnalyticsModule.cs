﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShivaCode
{

    public interface IAnalyticsModule
    {
        void AddAnalyticsKey(ShivaCode.Events.AnalyticsEvent analyticsEvent, string key, object obj);

        void FireAnalyticsEvent(ShivaCode.Events.AnalyticsEvent analyticsEvent);
    }

}
