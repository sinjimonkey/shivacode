﻿using System;
using UnityEngine.Events;

namespace ShivaCode.Events
{
    [Serializable]
    public class UnityVoidEvent : UnityEvent<Void>
    {
    }
}