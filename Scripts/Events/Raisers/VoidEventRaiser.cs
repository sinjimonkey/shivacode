﻿using System;
using ShivaCode.Debug;
using ShivaCode.ProcessWedges;
using Sirenix.OdinInspector;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace ShivaCode.Events
{
    public class VoidEventRaiser : BaseMonobehaviour
    {
        [field: SerializeField]
        public BaseGameEvent<Void> GameEvent { get; set; }
        public bool raiseOnStart = false;
        public bool Delay;
        [ShowIf("Delay"), Indent]
        public float DelayTime;
        public bool Repeating;
        private float Timer = 0;

        private void Start()
        {
            if (raiseOnStart) {
                LogHigher(ShivaDebug.LogLevel.Debug, "AutoRaising:  " + GameEvent);
                Raise();
            }
        }

        private void Update() {
            if (Repeating && Timer > 0) {
                Timer -= Time.deltaTime;
                if (Timer < 0) {
                    Raise();
                }
            }
        }

        private void Raise() {
            if (GameEvent != null)
                RaiseEvent();
            if (Repeating)
                Timer = DelayTime;
        }

        public virtual void RaiseEvent() {
            void act() => GameEvent?.Raise(new Void());
            if (!Delay)
                act();
            else
                CoroutineStarter.DelayFunction(act, DelayTime);
        }
    }
}