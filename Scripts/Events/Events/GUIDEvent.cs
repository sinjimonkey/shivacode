﻿using System;
using System.Collections;
using System.Collections.Generic;
using ShivaCode.Debug;
using ShivaCode.Events;
using ShivaCode.ProcessWedges;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Experimental.Audio;

namespace ShivaCode.Events
{
    [System.Serializable]
    public struct GUIDEventObject
    {
        public String GUID;
        public GameObject GameObject;
        public string EventName;
        public string ResultName;
        
        public object Result;
        public bool Success;

        public GUIDEventObject(String guid, GameObject gameObject, string eventName)
        {
            GUID = guid;
            GameObject = gameObject;
            EventName = eventName;

            Result = null;
            Success = false;
            ResultName = eventName + "_undefined";
        }

    }

    [CreateAssetMenu(fileName = "new GUID Event", menuName = "ShivaCode/Events/GUID Event")]
    public class GUIDEvent : BaseGameEvent<GUIDEventObject>
    {
        private static GUIDEvent _instance = null;
        private static List<GUIDEventObject> pending = new List<GUIDEventObject>();

        private static GUIDEvent Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;
                var t = Addressables.LoadAssetAsync<GUIDEvent>("ShivaCode/GUIDEvent");
                t.Completed += (res) =>
                {
                    if (res.Result == null)
                        return;
                    SetInstance(res.Result);

                    for (int i = pending.Count - 1; i >= 0; i--)
                    {
                        _instance.Raise(pending[i]);
                    }
                    
                    pending.Clear();

                    DontDestroyOnLoad(_instance);
                };
                
                return null;
            }
        }

        public static void SetInstance(GUIDEvent obj)
        {
            _instance = obj;
        }

        public static void Send(GUIDEventObject GUIDEventObject)
        {
            if (Instance == null)
            {
                pending.Add(GUIDEventObject);
            }
            else
                Instance.Raise(GUIDEventObject);
        }

        public static string NameWrap(GUIDEventObject GUID, bool success)
        {
            return $"{GUID.EventName}_{(success ? "success" : "fail")}_{GUID.GUID}";
        }
        
        public string LocalNameWrap(GUIDEventObject GUID, bool success)
        {
            return NameWrap(GUID, success);
        }

        public GUIDEventObject AutoNameWrap(GUIDEventObject GUID)
        {
            GUID.ResultName = NameWrap(GUID, GUID.Success);
            return GUID;
        }

        public static Action<T> GUIDWrap<T>(GUIDEventObject GUID, bool success = false)
        {
            return GUIDWrap<T>(GUID, (x) => { }, success);
        }

        public static Action<T> GUIDWrap<T>(GUIDEventObject GUID, Action<T> action, bool success = false)
        {
            if (GUID.Equals(default(GUIDEventObject)))
                return (result) =>
                {
                    UnityEngine.Debug.Log("Unwrapped GUID Event Return");
                    action(result);
                };

            GUID.Success = success;
            GUID.ResultName = NameWrap(GUID, success);
            
            
            return (result) =>
            {
                GUID.Result = result;
                UnityEngine.Debug.Log($"GUID Event {GUID.EventName}:  {GUID.Success}");
                action(result);
                Send(GUID);
            };
        }

        public void RaiseSpecial(GUIDEventObject GUID)
        {
            AutoNameWrap(GUID);
            Send(GUID);
        }
        public void SetAndRaise(GUIDEventObject GUID, object result, bool success)
        {
            GUID.Result = result;
            GUID.Success = success;
            RaiseSpecial(GUID);
        }

    }
}