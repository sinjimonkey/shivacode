﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Analytics;

namespace ShivaCode.Events
{
    [CreateAssetMenu(fileName = "new Analytics Event", menuName = "ShivaCode/Events/Analytics/Analytics Event")]
    [InlineEditor(),Serializable]
    public class AnalyticsEvent : DSSEvent
    {
        public override void Raise(Dictionary<string, object> item)
        {
            if (item == null)
                item = new Dictionary<string, object>();
            
            var result = UnityEngine.Analytics.AnalyticsEvent.Custom(name, item);
            String s = $"Raising Analytics Event:  {name} ({result})"+"\n";
            foreach (var v in item.Keys)
            {
                s += "\n" + $"  {v}:  {item[v]}";
            }
            
            UnityEngine.Debug.Log(s);

            base.Raise(item);
        }
    }
}