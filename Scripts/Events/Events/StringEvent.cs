﻿using System.Collections;
using System.Collections.Generic;
using ShivaCode.Events;
using UnityEngine;

[CreateAssetMenu(fileName = "new String Event", menuName = "ShivaCode/Events/String Event")]
public class StringEvent : BaseGameEvent<string>
{

}
