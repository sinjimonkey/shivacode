﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new String Analytic", menuName = "ShivaCode/Events/Analytics/String Analytics")]
public class AnalyticStringEvent : StringEvent
{
    public override void Raise(string item)
    {
        if (name.Contains("URL"))
            Application.OpenURL(item);
        
        var dict = new Dictionary<string, object>();
        dict.Add(name,item);
            
        var result = UnityEngine.Analytics.AnalyticsEvent.Custom(name, dict);
        Debug.Log($"AnalyticString Event:  {name}:  {item}  ({result})");

        base.Raise(item);
    }
}
