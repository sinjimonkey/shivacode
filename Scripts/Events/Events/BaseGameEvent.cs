﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using ShivaCode.Debug;
using ShivaCode.Interfaces;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UIElements;
using Bolt;

namespace ShivaCode.Events
{
    [InlineEditor(),Serializable]
    public class BaseGameEvent<T> : BaseScriptable, IReturnHandler<T>
    {
        [SerializeField]
        private readonly List<Action<T>> _editorCallbacks = new List<Action<T>>();
        [SerializeField]
        private readonly ListenerChain<T> _eventListeners = new ListenerChain<T>();

        public string EventName => name + "_Raised";
        
        public virtual void Raise(T item) {
            Log($"{typeof(T)} Event Raised:  { name} ({ item.ToString()})");
            if (_editorCallbacks != null)
            {
                for (int i = _editorCallbacks.Count - 1; i >= 0; i--)
                {
                    if (_editorCallbacks[i] == null)
                    {
                        _editorCallbacks.RemoveAt(i);
                        continue;
                    }

                    _editorCallbacks[i](item);
                }
            }

            _eventListeners?.EventRaised(item);

            var eventManager = Variables.Application.Get("ShivaEventManager") as GameObject;
            if (eventManager != null)
            {
                CustomEvent. Trigger(eventManager, EventName, new object[] {this, item});
            }

        }

        public void RegisterListener(IGameEventListener<T> listener)
        {
            _eventListeners.RegisterListener(listener);
        }

        public void UnregisterListener(IGameEventListener<T> listener)
        {
            _eventListeners.UnregisterListener(listener);
        }
        public void RegisterCallback(Action<T> callback)
        {
            _eventListeners.RegisterCallback(callback);
        }

        public void UnregisterCallback(Action<T> callback)
        {
            _eventListeners.RegisterCallback(callback);
        }

        public void Handle(T result)
        {
            Raise(result);
        }
    }
}