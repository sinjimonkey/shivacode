﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ShivaCode.Events
{
    [CreateAssetMenu(fileName = "new DictSS Event", menuName = "ShivaCode/Events/DictSS Event")]
    [InlineEditor(),Serializable]
    public class DSSEvent : BaseGameEvent<Dictionary<string,object>>
    {
        
    }
}