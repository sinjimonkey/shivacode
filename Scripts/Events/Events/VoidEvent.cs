﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ShivaCode.Events
{
    [CreateAssetMenu(fileName = "new Void Event", menuName = "ShivaCode/Events/Void Event")]
    [InlineEditor(),Serializable]
    public class VoidEvent : BaseGameEvent<Void>
    {
        [Button]
        public void Raise()
        {
            Raise(new Void());
        }
    }
}