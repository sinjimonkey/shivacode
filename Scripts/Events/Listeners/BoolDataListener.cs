﻿using System;
using System.Diagnostics;
using ShivaCode.ScriptableData;

namespace ShivaCode.Events
{
    public class BoolDataListener : BaseGameEventListener<String, BoolData, BoolDataEvent>
    {
        public override void EventRaised(string item)
        {
            var b = Boolean.Parse(item);
            UnityEngine.Debug.Log(name+" ReceivedRaise:  "+b);
            if (b)
                base.EventRaised(item);
        }   
    }
}