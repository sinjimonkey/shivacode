﻿using System;
using System.Collections.Generic;
using ShivaCode.Debug;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace ShivaCode.Events
{
    public abstract class BaseGameEventListener<T, TE, TUer> : BaseMonobehaviour, IGameEventListener<T>
        where TE : BaseGameEvent<T> where TUer : UnityEvent<T>
    {
        [field: SerializeField] public TE GameEvent { get; set; }
        [Space]
        [Space]
        [SerializeField] protected TUer unityEventResponse;
        [HideIf("IsVoid")]
        [SerializeField] protected UnityVoidEvent unityVoidResponse;
        
        private bool IsVoid => typeof(T) == typeof(Void);

        public virtual void EventRaised(T item)
        {
            if (GameEvent != null) {
                LogHigher(GameEvent.ShivaLogLevel, $"{typeof(T)} Event Received:  {name} [ {GameEvent.name} ({item.ToString()}) ]");
            }

            unityEventResponse?.Invoke(item);
            unityVoidResponse?.Invoke(new Void());
        }

        private void OnEnable()
        {
            if (GameEvent == null) return;

            GameEvent.RegisterListener(this);
        }

        private void OnDisable()
        {
            if (GameEvent == null) return;
            GameEvent.UnregisterListener(this);
        }
    }
}