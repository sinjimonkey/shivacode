﻿using ShivaCode.ScriptableData;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace ShivaCode.Events
{
    public class FunctionBranch : BaseMonobehaviour
    {
        [SerializeField] private BoolData _boolData;
        public UnityVoidEvent TrueResult;
        public UnityVoidEvent FalseResult;
        public void RaiseEvent()
        {
            //if (boolData == null || !boolData.boolData)
            //    GameEvent = falseListeners;
            //else
            //    GameEvent = trueListeners;
                
        }
    }
}