﻿namespace ShivaCode.Events
{
    public interface IGameEventListener<T>
    {
        void EventRaised(T item);
    }
}