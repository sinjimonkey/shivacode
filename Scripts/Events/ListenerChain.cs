﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ShivaCode.Events
{
    [Serializable]
    public class ListenerChain<T> : IGameEventListener<T>
    {
        [SerializeField]
        private readonly List<IGameEventListener<T>> _eventListeners = new List<IGameEventListener<T>>();
        [SerializeField]
        private readonly List<Action<T>> _eventCallbacks = new List<Action<T>>();

        public void EventRaised(T item)
        {
            for (var i = _eventListeners.Count - 1; i >= 0; i--) _eventListeners[i].EventRaised(item);
            for (var i = _eventCallbacks.Count - 1; i >= 0; i--) _eventCallbacks[i](item);
        }

        public void RegisterListener(IGameEventListener<T> listener)
        {
            if (!_eventListeners.Contains(listener))
                _eventListeners.Add(listener);
        }

        public void UnregisterListener(IGameEventListener<T> listener)
        {
            if (_eventListeners.Contains(listener))
                _eventListeners.Remove(listener);
        }
        public void RegisterCallback(Action<T> callback)
        {
            if (!_eventCallbacks.Contains(callback))
                _eventCallbacks.Add(callback);
        }

        public void UnregisterListener(Action<T> callback)
        {
            if (_eventCallbacks.Contains(callback))
                _eventCallbacks.Remove(callback);
        }

    }
}