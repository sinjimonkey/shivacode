﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using ShivaCode.Events;
using ShivaCode.Interfaces;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace ShivaCode.ScriptableData
{
    public abstract class ScriptableData : BaseGameEvent<string>
    {
        [NonSerialized]
        private string _stringData;
        
        [SerializeField]
        private List<BaseGameEventListener<string, BaseGameEvent<string>, UnityEvent<string>>> valueChanged = 
            new List<BaseGameEventListener<string, BaseGameEvent<string>, UnityEvent<string>>>();

        // ReSharper disable once MemberCanBeProtected.Global
        // ReSharper disable once InconsistentNaming
        
        public string stringData
        {
            get => _stringData;
            set
            {
                var b = value.Equals(_stringData);
                _stringData = value;
                if (!b)
                {
                    Raise(_stringData);
                }

            }
        }
        
        public virtual void Calculate()
        {
        }
    }
}