﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using ShivaCode.Debug;
using ShivaCode.Events;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Debug = System.Diagnostics.Debug;

namespace ShivaCode.ScriptableData
{
    
    [CreateAssetMenu(fileName = "new Bool Data", menuName = "ShivaCode/ScriptableData/Bool Data")]
    public class BoolData : ScriptableData
    {

        public static explicit operator bool(BoolData boolData)
        {
            return boolData.boolData;
        }

        // ReSharper disable once InconsistentNaming
        [ShowInInspector]
        public bool boolData
        {
            get => !string.IsNullOrWhiteSpace(stringData) && Boolean.Parse(stringData);
            protected set => stringData = value + "";
        }
        public void SetValue(bool value)
        {
            if (boolData == value)
                return;
            Log($"{name} Set:  {value}");
            boolData = value;

        }

        public void SetTrue(Void v = default)
        {
            SetValue(true);
        }
        public void SetFalse(Void v = default)
        {
            SetValue(false);
        }

        public bool isTrue => boolData;
        public bool isFalse => boolData;


    }
}