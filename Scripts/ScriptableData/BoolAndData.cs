﻿using System.Collections.Generic;
using ShivaCode.Events;
using UnityEngine;

namespace ShivaCode.ScriptableData
{
    
    [CreateAssetMenu(fileName = "new BoolAND", menuName = "ShivaCode/ScriptableData/BoolAND")]
    public class BoolAndData : BoolData, IGameEventListener<string>
    {
        public List<BoolData> comparators = new List<BoolData>();

        protected override void Initialize()
        {
            //UnityEngine.Debug.Log(name + "  Initialize");
            foreach (var data in comparators)
            {
                data.RegisterListener(this);
            }
            Calculate();
        }

        public void EventRaised(string item)
        {
            Calculate();
        }
        
        
        public override void Calculate()
        {
            base.Calculate();
            foreach (var comp in comparators)
            {
                comp.Calculate();
                if (!comp.boolData)
                {
                    SetFalse();
                    return;
                }
            }

            SetTrue();
        }

    }
}
