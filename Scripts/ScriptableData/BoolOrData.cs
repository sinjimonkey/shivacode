﻿using System.Collections.Generic;
using ShivaCode.Events;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace ShivaCode.ScriptableData
{
    
    [CreateAssetMenu(fileName = "new BoolOR", menuName = "ShivaCode/ScriptableData/BoolOR")]
    public class BoolOrData : BoolData, IGameEventListener<string>
    {
        public List<BoolData> comparators = new List<BoolData>();

        protected override void Initialize()
        {
                //UnityEngine.Debug.Log(name + "  Initialize");
                foreach (var boolData in comparators)
                {
                    boolData.RegisterListener(this);
                }
        }

        public void EventRaised(string item)
        {
            Calculate();
        }

        public override void Calculate()
        {
            base.Calculate();
            //UnityEngine.Debug.Log("Calculating...");
            foreach (var comp in comparators)
            {
                //UnityEngine.Debug.Log("\t"+comp.name+": "+comp.boolData);
                comp.Calculate();
                if (comp.boolData)
                {
                    SetTrue();
                    return;
                }
            }

            SetFalse();
        }
    }
}
