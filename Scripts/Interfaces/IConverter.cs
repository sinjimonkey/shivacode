﻿using System;
using ShivaCode.Events;

namespace ShivaCode.Interfaces
{
    public interface IConverter<in TI, out TO>
    {
        void Convert(TI input, Action<TO> output, Action<string> error);
    }
}