﻿using UnityEngine;

namespace ShivaCode.Interfaces
{
    public interface IReturnHandler<in T>
    {
        void Handle(T result);
    }
}
