﻿using System;
using ShivaCode.Events;
using UnityEngine;

namespace ShivaCode.Interfaces
{
    public interface IGUIDReturnHandler<in T> : IReturnHandler<T>
    {
        Action<T> GUIDHandle(GUIDEventObject GUID, T result);
    }
}