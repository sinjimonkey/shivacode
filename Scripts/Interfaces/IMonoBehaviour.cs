﻿using UnityEngine;

namespace ShivaCode.Interfaces
{
    public interface IMonoBehaviour
    {
        void SetActive(bool b);
        IMonoBehaviour Instantiate();
        GameObject GameObject { get; }
    }
}