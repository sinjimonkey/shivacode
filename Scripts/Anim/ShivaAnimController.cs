﻿using System.Collections;
using System.Collections.Generic;
using ShivaCode;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ShivaAnimController : BaseMonobehaviour
{
    private Animator Animator => GameObject.GetComponent<Animator>();

    public void SetAnimatorInt(string key, int value) => Animator.SetInteger(key, value);
    public int GetAnimatorInt(string key) => Animator.GetInteger(key);

    public void SetAnimatorFloat(string key, float value) => Animator.SetFloat(key, value);
    public float GetAnimatorFloat(string key) => Animator.GetFloat(key);

    public void SetAnimatorBool(string key, bool value) => Animator.SetBool(key, value);
    public bool GetAnimatorBool(string key) => Animator.GetBool(key);

    public void SetAnimatorTrigger(string key) => Animator.SetTrigger(key);
}
