﻿using System.Collections;
using System.Collections.Generic;
using ShivaCode;
using ShivaCode.Debug;
using ShivaCode.Interfaces;
using UnityEngine;

public class DebugDisplay : BaseMonobehaviour, IReturnHandler<DebugItem>
{
    public Transform Parent;
    public DebugDisplayItem DisplayItem;

    public void Handle(DebugItem result) {
        var clone = Instantiate<DebugDisplayItem>(DisplayItem, Parent);
        clone.SetText(result);
        clone.SetActive(true);
    }
}
