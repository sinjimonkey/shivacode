﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShivaCode;

namespace ShivaCode.Debug {

    public class SetDebugHandler : BaseMonobehaviour {
        public ShivaDebug shivaDebug;
        public float timer = 0;
        void Awake() {
            if (shivaDebug != null)
            {
                shivaDebug.SetInstance();
                timer = shivaDebug.saveDelay;
            }

        }

        private void Update()
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
                if (timer < 0)
                {
                    timer += shivaDebug.saveDelay;
                    shivaDebug.TrySave();
                }
            }
        }
    }
}
