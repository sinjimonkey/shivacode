﻿using System;
using System.Collections;
using System.Collections.Generic;
using ShivaCode.Debug;
using ShivaCode.Events;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "Debug Event", menuName = "ShivaCode/Events/Debug Event")]
public class DebugEvent : BaseGameEvent<ShivaCode.Debug.DebugItem> {
    public override void Raise(DebugItem item) {
        ShivaLogLevel = ShivaDebug.LogLevel.None;   // no loops!
        base.Raise(item);
    }
}

[Serializable]
public class DebugResponce : UnityEvent<ShivaCode.Debug.DebugItem> {
}
