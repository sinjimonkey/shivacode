﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShivaCode;
using Sirenix.OdinInspector;
using System;
using Debug = UnityEngine.Debug;
using ShivaCode.Events;
using ShivaCode.ProcessWedges;

namespace ShivaCode.Debug {
    public struct DebugItem {
        public string Message;
        public ShivaDebug.LogLevel Level;
    }
    [CreateAssetMenu(fileName = "Shiva Debug Parameters", menuName = "ShivaCode/ScriptableData/ShivaDebug")]
    public class ShivaDebug : BaseScriptable {
        public static ShivaDebug instance = null;

        private static readonly List<Action> ActionList = new List<Action>();

        private static void CatchUp() {
            if (ActionList.Count > 0) {
                var n = ActionList.Count - 1;
                var s = ActionList[n];
                ActionList.RemoveAt(n);
                s(); // this will recurse and wind up desplaying the one at 0 first.
            }
        }

        public static void MasterLog(LogLevel log, string message, bool instant = true)
        {
            if (message == null) return;
            if (log == LogLevel.None || message.ToLower().Contains("debug"))
                return;
            if (instant) {
                switch (log) {
                    case LogLevel.Debug:
                        UnityEngine.Debug.Log($"* {message}");
                        break;
                    case LogLevel.Warning:
                        UnityEngine.Debug.LogWarning($"* {message}");
                        break;
                    case LogLevel.Error:
                        UnityEngine.Debug.LogError($"* {message}");
                        break;
                    default: 
                        break;
                }
            }
            if (instance == null) {
                ActionList.Add(() => MasterLog(log, message));
                return;
            }

            CatchUp();  // catch up on old messages

            instance.Log(log, message);
        }

        public enum LogLevel {
            None,
            LogOnly,
            Verbose,
            Debug,
            Warning,
            Error
        }

        public BaseGameEvent<DebugItem> LogEvent = null;
        public bool DoDebug = false;
        [ShowIf("DoDebug"), Indent]
        public LogLevel RaiseLevel;
        public bool RaiseUnityLogs = false;
        public LogLevel saveLogLevel = LogLevel.Debug;
        public float saveDelay = 10;
        
        [NonSerialized]
        private string SessionLog = "";

        public void SetInstance() {
            if (instance != null)
                Application.logMessageReceived -= instance.LogMessageReceived;
            instance = this;
            Application.logMessageReceived += LogMessageReceived;
            MasterLog(LogLevel.Debug, "Debug Constole Initialized", true);
            CatchUp();
        }

        private void Log(LogLevel log, string message) {
            Log(new DebugItem() { Level = log, Message = message });
        }

        private void Log(DebugItem item){
            // First we want to add it to the session log
            AddSessionLog(item);
            if (RaiseUnityLogs)
                return;
            if (!DoDebug || (int)item.Level < (int)RaiseLevel)
                return;
            if (LogEvent != null)
                LogEvent.Raise(item);
        }

        public void LogMessageReceived(string condition, string trace, LogType type ) {
            LogLevel log = LogLevel.Debug;
            if (type == LogType.Warning || type == LogType.Assert)
                log = LogLevel.Warning;
            else if (type == LogType.Error || type == LogType.Exception)
                log = LogLevel.Error;
            var item = new DebugItem() { Level = log, Message = condition };
            AddSessionLog(item);

            if (RaiseUnityLogs && LogEvent != null) {
                LogEvent.Raise(item);
            }
        }

        private bool dirty = false;

        private void AddSessionLog(DebugItem item)
        {
            if (item.Level < saveLogLevel)
                return;
            if (Application.isPlaying) {
                if (item.Message.Contains("SessionLog"))
                    return;
                //UnityEngine.Debug.LogWarning("AddSessionLog:  " + item.Message);
                if ((int)item.Level > (int)LogLevel.Debug) // Displays WARNING:  or ERROR: 
                    SessionLog += (item.Level.ToString().ToUpper()) + ":  ";

                SessionLog += item.Message + "\r";
                dirty = true;
            }
        }

        private void SaveSessionLog()
        {
            if (true)
                return;
            if (!dirty || saveDelay == 0)
                return;
            if (Application.isPlaying)
            {
                UnityEngine.Debug.LogWarning("SessionLogSave:  ");//+SessionLog);
                DeviceWrapper.DeviceWrapper.DoWithDevice(device => {
                    try
                    {
                        device.WriteFile("UnityLog.txt", "Augle Log:  "+DateTime.Now+"\r"+SessionLog);
                        dirty = false;
                    }
                    catch (Exception e)
                    { 
                        LogError("Write File Error:  "+e.Message);
                        SaveSessionLog();
                    }
                    
                });
            }
        }

        public void TrySave() {
            if (dirty)
                SaveSessionLog();
        }
    }
}
