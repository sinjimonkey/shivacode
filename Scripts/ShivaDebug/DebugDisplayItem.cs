﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShivaCode;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;
using ShivaCode.Debug;

public class DebugDisplayItem : BaseMonobehaviour
{
    [BoxGroup("Time")] public float showTime = 10;
    [BoxGroup("Time")] public float warningTime = 15;
    [BoxGroup("Time")] public float errorTime = 30;
    public DebugItem item;
    [ShowInInspector, SerializeField]
    private Text textField;
    private float aliveTime = 0;

    private void SetText(string text) {
        textField.text = text;
        if (item.Level == ShivaDebug.LogLevel.Warning)
            aliveTime = warningTime;
        else if (item.Level == ShivaDebug.LogLevel.Error)
            aliveTime = errorTime;
        else
            aliveTime = showTime;
    }

    internal void SetText(DebugItem result) {
        item = result;
        if (result.Level == ShivaDebug.LogLevel.Warning)
            SetText(result.Message, Color.yellow);
        else if (result.Level == ShivaDebug.LogLevel.Error)
            SetText(result.Message, Color.red);
        else 
            SetText(result.Message);
    }

    public void SetText(string text, Color color) {
        SetText(text);
        textField.color = color;
    }

    private void Update() {
        aliveTime -= Time.deltaTime;
        if (aliveTime <= 0)
            Destroy(this.gameObject);
    }
}
