﻿using System.Collections;
using System.Collections.Generic;
using ShivaCode.Debug;
using ShivaCode.Events;
using UnityEngine;

public class DebugListener : BaseGameEventListener<ShivaCode.Debug.DebugItem, DebugEvent, DebugResponce> {
    public override void EventRaised(DebugItem item) {
        ShivaLogLevel = ShivaDebug.LogLevel.None;   // no loops!
        base.EventRaised(item);
    }
}
