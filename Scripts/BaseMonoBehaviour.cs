﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ShivaCode.Debug;
using ShivaCode.Events;
using ShivaCode.Interfaces;
using ShivaCode.ProcessWedges;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using ShivaCode.Events;
using UnityEngine.Analytics;
using ShivaCode;

namespace ShivaCode
{
    public class BaseMonobehaviour : SerializedMonoBehaviour, IMonoBehaviour, IAnalyticsModule
    {
        
        
        [FoldoutGroup("Base")]
        [PropertyOrder(9001)]
        [SerializeField] public ShivaDebug.LogLevel ShivaLogLevel = ShivaDebug.LogLevel.Debug;
        [TextArea]
        [FoldoutGroup("Base")]
        [PropertyOrder(9001)]
        [SerializeField] private string reminders;

        protected bool baseDone;
        
        #region IMonoBehaviour Implementation
        public void SetActive(bool b)
        {
            if (IsGraphicsLoop())
            {
                DoAfterSeconds(0.01f, () => SetActive(b));
                return;
            }

            gameObject.SetActive(b);
        }

        public string ParentTrace
        {
            get
            {
                string s = this.name;
                var t = this.transform;

                while (t.parent != null)
                {
                    t = t.parent;
                    s = t.name + "/" + t;
                }

                return s;
            }
        }

        public IMonoBehaviour Instantiate()
        {
            return Instantiate(this);
        }

        public GameObject GameObject => gameObject;

        #endregion


        public bool BaseDone() => baseDone;
        
        private void OnDestroy()
        {
            foreach (var field in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                Type fieldType = field.FieldType;
 
                if (typeof(IList).IsAssignableFrom(fieldType))
                {
                    var list = field.GetValue(this) as IList;
                    list?.Clear();
                }
 
                if (typeof(IDictionary).IsAssignableFrom(fieldType))
                {
                    IDictionary dictionary = field.GetValue(this) as IDictionary;
                    dictionary?.Clear();
                }
 
                if (!fieldType.IsPrimitive)
                {
                    field.SetValue(this, null);
                }
            }
        }
        
        protected void Awake()
        {
#if UNITY_EDITOR
            UnityEngine.Debug.unityLogger.logEnabled = true;
#else
            UnityEngine.Debug.unityLogger.logEnabled = false;
#endif
            Initialize();
        }

        public virtual void OnValidate()
        {
            Initialize();
        }

        protected virtual void Initialize()
        {
            
        }

        protected ShivaCanvas GetParentCanvas() {
            var test = this.transform;
            while (test != null) {
                var v = test.gameObject.GetComponent<ShivaCanvas>();
                if (v != null)
                    return v;
                test = test.parent;
            }
            UnityEngine.Debug.LogWarning("Null ShivaCanvas:  "+ParentTrace);
            return null;
        }

        public void RebuildRects()
        {
            RebuildRects(GameObject);
        }

        protected void RebuildRects(GameObject go)
        {
            while (true)
            {
                var rect = go.GetComponent<RectTransform>();
                if (go != null)
                {
                    LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
                }

                if (go.transform.parent != null)
                {
                    go = go.transform.parent.gameObject;
                    continue;
                }

                break;
            }
        }

        public void Log(string message) {
            ShivaDebug.MasterLog(ShivaLogLevel, message);
        }
        public void LogWarning(string message) {
            ShivaDebug.MasterLog(ShivaDebug.LogLevel.Warning, message);
        }
        public void LogError(string message) {
            ShivaDebug.MasterLog(ShivaDebug.LogLevel.Error, message);
        }
        public void LogHigher(ShivaDebug.LogLevel level, string message) {
            var lx = ShivaLogLevel;
            if ((int)level > (int)lx)
                lx = level;
            ShivaDebug.MasterLog(lx, message);
        }

        public List<Transform> ParentList() {
            var pre = new List<Transform>();
            var t = this.transform.parent;
            while (t != null) {
                pre.Add(t);
                t = t.parent;
            }

            var ret = new List<Transform>();
            while (pre.Count > 0) { // flip it
                ret.Add(pre[pre.Count - 1]);
                pre.RemoveAt(pre.Count - 1);
            }

            return ret;
        }

        public void DoAfterSeconds(float time, Action act) {
            CoroutineStarter.DelayFunction(act, time);
        }

        public bool IsGraphicsLoop()
        {
            if (UnityEngine.UI.CanvasUpdateRegistry.IsRebuildingGraphics() ||
                UnityEngine.UI.CanvasUpdateRegistry.IsRebuildingLayout())
                return true;

            return false;
        }


        private Dictionary<string, Dictionary<string, object>> _eventBuilder;
        
        public void AddAnalyticsKey(ShivaCode.Events.AnalyticsEvent analyticsEvent, string key, object obj)
        {
            if (_eventBuilder == null)
                _eventBuilder = new Dictionary<string, Dictionary<string, object>>();
            
            Dictionary<string, object> dict;
            if (_eventBuilder.ContainsKey(analyticsEvent.name))
                dict = _eventBuilder[analyticsEvent.name];
            else
            {
                dict = new Dictionary<string, object>();
                _eventBuilder.Add(analyticsEvent.name, dict);
            }

            if (dict.ContainsKey(key))
                dict[key] = obj;
            else 
                dict.Add(key, obj);
        }
        
        public void FireAnalyticsEvent(ShivaCode.Events.AnalyticsEvent analyticsEvent) {
            UnityEngine.Debug.Log("Here I am");
            if (_eventBuilder.ContainsKey(analyticsEvent.name))
            {
                UnityEngine.Debug.Log("EventBuilder");
                var res = _eventBuilder[analyticsEvent.name];
                _eventBuilder.Remove(analyticsEvent.name);
                DoAfterSeconds(0.1f, () => analyticsEvent.Raise(res));
                
            }
            else
            {
                UnityEngine.Debug.Log("SingleEvent");
                DoAfterSeconds(0.1f, () => new Dictionary<string, object>());
            }
        }
    }
}