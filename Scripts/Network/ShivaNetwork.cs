﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using ShivaCode.ProcessWedges;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace ShivaCode.Network
{
    public static class ShivaNetwork
    {

        private static INetworkGetMOTD MOTDData { get; set; }
        private static INetworkGetTitleData TitleData { get; set; }
        private static INetworkGetPlayerData PlayerData { get; set; }

        private static readonly List<Action> Pending = new List<Action>();

        public static void SetServer(INetworkGetMOTD value)
        {
            MOTDData = value;
            RunPending();
        }

        public static void SetServer(INetworkGetTitleData value)
        {
            TitleData = value;
            RunPending();
        }

        public static void SetServer(INetworkGetPlayerData value)
        {
            PlayerData = value;
            RunPending();
        }

        private static void RunPending()
        {
            for (int i = Pending.Count - 1; i >= 0; i--)
            {
                Pending[i]?.Invoke();
                Pending.RemoveAt(i);
            }
        }

        public static void GetMOTD(Action<Dictionary<string, string>> callback, Action<string> errorCallback)
        {
            if (MOTDData == null)
            {
                UnityEngine.Debug.LogWarning("MOTD is null");
                
                Pending.Add(()=>GetMOTD(callback, errorCallback));
                return;
            }

            MOTDData.GetNetworkMOTD(callback, errorCallback);
        }

        public static void GetTitleData(string key, Action<string> callback, Action<string> errorCallback)
        {
            if (TitleData == null)
            {
                UnityEngine.Debug.LogWarning("TitleData is null");
                
                Pending.Add(()=>GetTitleData(key, callback, errorCallback));
                return;
            }
            TitleData.GetNetworkTitleKey(key, callback, errorCallback);
        }

        public static void GetTitleData(IEnumerable<string> keys, Action<Dictionary<string, string>> callback, Action<string> errorCallback)
        {
            if (TitleData == null)
            {
                UnityEngine.Debug.LogWarning("TitleData is null");
                
                Pending.Add(()=>GetTitleData(keys, callback, errorCallback));
                return;
            }
            TitleData.GetNetworkTitleKeys(keys, callback, errorCallback);
        }
    }
}
