﻿using System;
using System.Collections.Generic;

namespace ShivaCode.Network
{
    public interface INetworkGetTitleData
    {
        void GetNetworkTitleKey(string path, Action<string> callback, Action<string> errorCallback);
        void SetNetworkTitleKey(string path, string text, bool noBackup = true);
        void GetNetworkTitleKeys(IEnumerable<string> paths, Action<Dictionary<string,string>> callback, Action<string> errorCallback);
    }
}