﻿using System;
using UnityEngine;

namespace ShivaCode.Network
{
    public interface INetworkGetBundle {
        void GetBundle(string path, Action<AssetBundle> callback, Action<string> errorCallback);
        void GetObjects(string path, Action<UnityEngine.Object[]> callback, Action<string> errorCallback);
        void SaveBundle(string path, Action<bool> callback, Action<string> errorCallback);
    }
}