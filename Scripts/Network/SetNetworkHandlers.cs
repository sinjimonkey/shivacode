﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace ShivaCode.Network
{
    public class SetNetworkHandlers : BaseMonobehaviour
    {
        [SerializeField, ShowInInspector] public INetworkGetMOTD motd;
        [SerializeField, ShowInInspector] public INetworkGetPlayerData player;
        [SerializeField, ShowInInspector] public INetworkGetTitleData title;

        public void Set()
        {
            if (motd != null)
                ShivaNetwork.SetServer(motd);
            if (player != null)
                ShivaNetwork.SetServer(player);
            if (title != null)
                ShivaNetwork.SetServer(title);
        }
    }
}
