﻿using System;
using ShivaCode.Events;
using ShivaCode.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ShivaCode.Network
{
    public class BundleExtractor : IConverter<AssetBundle, UnityEngine.Object[]>
    {
        public static void ConvertBundleToObjectArrayExternal(AssetBundle bundle, GUIDEventObject GUID)
        {
            Extract(bundle, GUIDEvent.GUIDWrap<Object[]>(GUID, true), GUIDEvent.GUIDWrap<string>(GUID));
        }

        public static void Extract(AssetBundle bundle, System.Action<Object[]> output, Action<string> error = null)
        {
            var extractor = new BundleExtractor();
            extractor.Convert(bundle, output, error);
        }
        public void Convert(AssetBundle bundle, System.Action<Object[]> output, Action<string> error = null)
        {
            if (bundle == null) {
                output(new Object[0]);
                error?.Invoke("Null Conversion AssetBundle - Object[]");
                return;
            }
            var assetObjectArray = bundle.LoadAllAssets();
            output(assetObjectArray);
        }

        public void Convert(AssetBundle bundle, System.Action<Object[]> output, GUIDEventObject guidEventObject)
        {
            Convert(bundle, GUIDEvent.GUIDWrap(guidEventObject, output));
        }
    }
}