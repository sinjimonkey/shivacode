﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using ShivaCode.ProcessWedges;
using ShivaCode.ScriptableData;
using UnityEngine;
using static ShivaCode.DeviceWrapper.DeviceWrapper;


namespace ShivaCode.Network
{
    [Serializable]
    [CreateAssetMenu(fileName = "LocalIO", menuName = "ShivaCode/Network/LocalIO")]
    public class LocalDiskIO : BaseScriptable, INetworkGetTitleData, INetworkGetPlayerData, INetworkGetBundle
    {
        public BoolData asyncBlocked;
        public static string SanitizePath(string path)
        {
            return DeviceWrapper.DeviceWrapper.SanitizePath(path);
        }
        private static void ReadFromFile(string path, Action<string> callback, Action<string> errorCallback)
        {
            UnityEngine.Debug.Log("ReadFromFile:  "+path);
            try
            {
                DoWithDevice((device) =>
                {
                    UnityEngine.Debug.Log("DoWithDevice-ReadFromFile:  "+path);
                    device.ReadFile(path, callback, errorCallback);
                });
            }
            catch (Exception e)
            {
                errorCallback(e.Message);
            }
        }
        

        private static void WriteToFile(string path, string text, bool noBackup = true)
        {
            UnityEngine.Debug.Log("WriteToFile:  "+path);
            DoWithDevice((device) =>
            {
                UnityEngine.Debug.Log("DoWithDevice-WriteToFile:  "+path);
                device.CreateDirectories(path);
                device.CreateFile(path);
                device.WriteFile(path, text, noBackup);
            });   
        }

        public void GetNetworkTitleKey(string path, Action<string> callback, Action<string> errorCallback)
        {
            ReadFromFile(Path.Combine("TitleData",path), callback, errorCallback);
        }
        public void SetNetworkTitleKey(string path, string text, bool noBackup = true)
        {
            WriteToFile(Path.Combine("TitleData",path), text, noBackup);
        }
        public void GetNetworkPlayerKey(string path, Action<string> callback, Action<string> errorCallback)
        {
            ReadFromFile(Path.Combine("PlayerData",path), callback, errorCallback);
        }
        public void SetNetworkPlayerKey(string path, string text, bool noBackup = true)
        {
            WriteToFile(Path.Combine("PlayerData",path), text, noBackup);
        }

        public void GetNetworkTitleKeys(IEnumerable<string> paths, Action<Dictionary<string, string>> callback, Action<string> errorCallback)
        {
            throw new NotImplementedException();
        }

        public void GetNetworkPlayerKeys(IEnumerable<string> paths, Action<Dictionary<string, string>> callback, Action<string> errorCallback)
        {
            throw new NotImplementedException();
        }

        private void GetSyncedBundle(string sanPath, Action<AssetBundle> callback, Action<string> errorCallback) {
            AssetBundleCreateRequest request;
            
            AssetBundle result = null;
            result = AssetBundle.LoadFromFile(sanPath);

            UnityEngine.Debug.Log("Loading Done (Sync):  " + sanPath);
            if (result == null)
                errorCallback("No bundle (Sync)!?  " + sanPath);

            callback.Invoke(result);
        }

        private void GetAsyncedBundle(string sanPath, Action<AssetBundle> callback, Action<string> errorCallback) {
            CoroutineStarter.AddCoroutine(Load());

            IEnumerator Load() {
                AssetBundleCreateRequest request;
                try {
                    request = AssetBundle.LoadFromFileAsync(sanPath);
                    request.allowSceneActivation = true;
                    
                    request.priority++;
                } catch (Exception e) {
                    errorCallback(e.Message);
                    callback(null);
                    yield break;
                }

                var counter = 0;

                AssetBundle result = null;
                request.allowSceneActivation = true;
                float prior = 0;
                
                
                ProgressTracker.BundleProgress.AddBit(new ProgressTracker.ProgressBit(()=>request.progress,100,"Load"));
                
                while (!request.isDone) {
                    UnityEngine.Debug.Log($"{request.progress} ({counter})");
                    yield return new WaitForSeconds(0.1f);
                    if (request.progress > prior) counter = 0;
                    else counter++;

                    prior = request.progress;
                    if (request.progress >= 0.89)
                    {
                        if (counter == 5)
                        {
                            request.allowSceneActivation = true;
                        }

                        if (counter == 10) // it's not actually loading
                        {
                            request.priority -= 100;
                            UnityEngine.Debug.Log("Switching to Sync");
                            GetSyncedBundle(sanPath, callback, errorCallback);
                            yield break;
                        }
                    }
                }

                if (result == null && request != null)
                    result = request.assetBundle;

                UnityEngine.Debug.Log("Loading Done (Async):  " + sanPath);
                yield return new WaitForSeconds(0.1f);
                if (result == null)
                    errorCallback("No bundle!?  " + sanPath);

                callback.Invoke(result);
            }
            
        }

        public void GetBundle(string path, Action<AssetBundle> callback, Action<string> errorCallback)
        {
            var sanPath = SanitizePath(path);
            try {
                sanPath = PrependDataPath(sanPath, persistent: true);
            } catch (ArgumentException) {
                UnityEngine.Debug.LogError("Invalid path:  " + sanPath);
            }
            
            UnityEngine.Debug.Log("Loading:  "+sanPath);
            DoWithDevice(device =>
            {
                if (!device.CheckFileExists(sanPath))
                {
                    UnityEngine.Debug.Log("File does not exist:  " + sanPath);
                    callback(null);
                    return;
                }

                if (asyncBlocked == null || !asyncBlocked.boolData)
                    GetAsyncedBundle(sanPath, callback, errorCallback);
                else
                    GetSyncedBundle(sanPath, callback, errorCallback);                
            });
        }
        
        

        public void SaveBundle(string path, Action<bool> callback, Action<string> errorCallback)
        {
            path = DeviceWrapper.DeviceWrapper.SanitizePath(path);
            // we'll check if it exists
            DoWithDevice(device => { callback (device.CheckFileExists(path, true)); });
        }

        public void GetObjects(string path, Action<UnityEngine.Object[]> callback, Action<string> errorCallback) {
            IEnumerator asyncExtract(AssetBundle bundle) {
                var v = bundle.LoadAllAssetsAsync();
                yield return v;
                callback(v.allAssets);
            }

            void doBundle(AssetBundle bundle) {
                CoroutineStarter.AddCoroutine(asyncExtract(bundle));
            }
            
            GetBundle(path, doBundle, errorCallback);
        }
    }
}
