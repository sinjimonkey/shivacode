﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using ShivaCode;
using ShivaCode.Debug;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "new Email Handler", menuName = "ShivaCode/Network/Email Handler")]
public class ShivaEmailHandler : BaseScriptable {
    [SerializeField,BoxGroup("SMTP")] private string smtpAccount;
    [ShowInInspector, BoxGroup("SMTP")] private bool showPass = false;
       [SerializeField, BoxGroup("SMTP"), ShowIf("showPass"), Indent] private string credential;
    [SerializeField, BoxGroup("SMTP")] private string smtpServer = "smtp.gmail.com";
    [SerializeField, BoxGroup("SMTP")] private int smtpPort = 587;
    [SerializeField, BoxGroup("SMTP")] private bool EnableSsl = true;

    [SerializeField, BoxGroup("Recipients")] private string to;
    [SerializeField, BoxGroup("Recipients")] private string[] cc;
    [SerializeField, BoxGroup("Recipients")] private string[] bcc;

    public void SendMail(string subject, string body, MailAddress replyTo = default) {
        try {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(smtpAccount);
            mail.To.Add(to);
            foreach (var s in cc)
                mail.CC.Add(s);
            foreach (var s in bcc)
                mail.Bcc.Add(s);
            mail.Subject = subject;
            mail.Body = body;
            if (replyTo != default) 
                mail.ReplyToList.Add(replyTo);


            SmtpClient server = new SmtpClient(smtpServer, smtpPort);
            //server.Port = smtpPort;
            server.EnableSsl = EnableSsl;
            server.Timeout = 20000;

            server.DeliveryMethod = SmtpDeliveryMethod.Network;
            
            server.Credentials = new NetworkCredential(smtpAccount, credential) as ICredentialsByHost;
            ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            server.Send(mail);
            ShivaDebug.MasterLog(ShivaDebug.LogLevel.Debug, $"Message Sent:   {subject}");

        } catch (Exception e) {
            ShivaDebug.MasterLog(ShivaDebug.LogLevel.Error, $"Shiva Mail Error:   {e.Message}");
        }
    }
}
 