﻿using System;
using System.Collections;
using System.Collections.Generic;
using ShivaCode.Interfaces;
using ShivaCode.ProcessWedges;
using ShivaCode.ScriptableData;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.Video;

namespace ShivaCode.Network 
{
    [Serializable]
    [CreateAssetMenu(fileName = "AddressableIO", menuName = "ShivaCode/Network/AddressableIO")]
    public class AddressableIO : BaseScriptable, INetworkGetBundle {

        public BoolData asyncBlocked;

        public void GetBundle(string path, Action<AssetBundle> callback, Action<string> errorCallback) {
            string sanPath = path.ToLower();
            if (!sanPath.Contains("internal") || sanPath.Contains(".")) {
                callback(null);
                return;
            }
            IEnumerator getBundleAsync() {
                var v = Addressables.LoadAssetAsync<AssetBundle>(sanPath);
                yield return v;
                callback(v.Result);
            }
            CoroutineStarter.AddCoroutine(getBundleAsync());
        }
        
        public void GetObjects(string path, Action<UnityEngine.Object[]> callback, Action<string> errorCallback) {
            string sanPath = path.ToLower();
            if (!sanPath.Contains("internal") || sanPath.Contains(".")) {
                callback(new UnityEngine.Object[0]);
                return;
            }

            UnityEngine.Debug.Log("Getting internal address:  "+sanPath);
            IEnumerator getBundleAsync() {
                UnityEngine.Debug.Log("Addressable search:  " + sanPath);
                var v = Addressables.LoadAssetAsync<UnityEngine.Object>(sanPath);
                yield return v;
                if (v.Result == null) {
                    UnityEngine.Debug.Log("?? : " + null);
                    callback(new UnityEngine.Object[0]);
                } else {
                    UnityEngine.Debug.Log("!! : " + v.Result.name);
                    callback(new UnityEngine.Object[] { v.Result, });
                }
            }
            CoroutineStarter.AddCoroutine(getBundleAsync());
        }

        public void SaveBundle(string path, Action<bool> callback, Action<string> errorCallback) {
            
        }
    }
}
