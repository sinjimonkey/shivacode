﻿using System;
using System.Collections.Generic;

namespace ShivaCode.Network
{
    public interface INetworkGetMOTD
    {
        void GetNetworkMOTD(Action<Dictionary<string, string>> callback, Action<string> errorCallback);
    }
}