﻿using System;
using System.Collections;
using System.Collections.Generic;
using ShivaCode.ProcessWedges.NetworkWedges;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace ShivaCode.Network
{
    public enum DataType
    {
        PlayerData,
        TitleData
    }
}