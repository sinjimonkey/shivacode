﻿using System;
using System.Collections;
using ShivaCode.Events;
using ShivaCode.Interfaces;
using ShivaCode.ProcessWedges;
using UnityEngine;
using UnityEngine.Networking;
using Debug = UnityEngine.Debug;

namespace ShivaCode.Network
{
    [CreateAssetMenu(fileName =  "New Bundle Getter", menuName = "ShivaCode/Network/Bundle Getter")]
    public class BundleGetter : ProcessWedge<AssetBundle>, IConverter<string, AssetBundle>
    {
        [SerializeField] private INetworkGetBundle network;
        [SerializeField] private INetworkGetBundle localIO;
        [SerializeField] private string bundlePath;
        private Action<AssetBundle> _callback;
        private Action<string> _errorCallback;

        private bool _lock = false;
        
        public void SetNetworks(INetworkGetBundle net, INetworkGetBundle local)
        {
            network = net;
            localIO = local;
        }


        public void GetBundle(string path, Action<AssetBundle> callback, Action<string> errorCallback)
        {
            bundlePath = path;
            _callback = callback;
            _errorCallback = errorCallback;
            Setup();
        }

        public override void Setup(GUIDEventObject guidEventObject = default)
        {
            
            base.Setup(guidEventObject);
            void LoadReturn(AssetBundle bundle)
            {
                if (bundle != null)
                    FinishSuccess(bundle);
                else
                    Start();
            }

            void LoadError(string s)
            {
                UnityEngine.Debug.LogError("Load Error... Attempting load;\n"+s);
                Start();
            }


            UnityEngine.Debug.Log("BundleGetter:  "+bundlePath);
            localIO.GetBundle(bundlePath, LoadReturn, LoadError);
        }

        public override void Start()
        {
            base.Start();
            UnityEngine.Debug.Log("Downloading start:  "+bundlePath);

            network.GetBundle(bundlePath, FinishSuccess ,_errorCallback);
        }

        public override void Update()
        {
            
        }

        public new void FinishFail(string s)
        {
            _errorCallback?.Invoke(s);
            base.FinishFail(s);
            _lock = false;
        }

        

        public override void FinishSuccess(AssetBundle bundle)
        {
            _callback?.Invoke(bundle);
            base.FinishSuccess(bundle);
            _lock = false;
        }

        // Converts from the cdn or disk path to the AssetBundle
        public void Convert(string path, Action<AssetBundle> output, Action<string> error = null)
        {
            if (_lock)
            {
                CoroutineStarter.DelayFunction(()=>Convert(path,output, error), 0.5f);
            }

            _lock = true;

            //Input is a path
            bundlePath = path;
            _callback = output;
            Setup();
        }

    }
}