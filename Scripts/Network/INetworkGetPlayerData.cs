﻿using System;
using System.Collections.Generic;

namespace ShivaCode.Network
{
    public interface INetworkGetPlayerData
    {
        void GetNetworkPlayerKey(string path, Action<string> callback, Action<string> errorCallback);
        void SetNetworkPlayerKey(string path, string text, bool noBackup = true);
        void GetNetworkPlayerKeys(IEnumerable<string> paths, Action<Dictionary<string,string>> callback, Action<string> errorCallback);

    }
}