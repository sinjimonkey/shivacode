﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using ShivaCode.Debug;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ShivaCode
{
    public abstract class BaseScriptable : SerializedScriptableObject
    {
        
        [FoldoutGroup("Base")]
        protected bool baseDone = false;
        
        [TextArea, PropertyOrder(9001), HideInInlineEditors, SerializeField]
        [FoldoutGroup("Base")]
        private string _reminders;
        [FoldoutGroup("Base")]
        [SerializeField] public ShivaDebug.LogLevel ShivaLogLevel = ShivaDebug.LogLevel.Verbose;

        public bool BaseDone() => baseDone;
        void OnDestroy()
        {
            foreach (FieldInfo field in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                Type fieldType = field.FieldType;
 
                if (typeof(IList).IsAssignableFrom(fieldType))
                {
                    IList list = field.GetValue(this) as IList;
                    if (list != null)
                    {
                        list.Clear();
                    }
                }
 
                if (typeof(IDictionary).IsAssignableFrom(fieldType))
                {
                    IDictionary dictionary = field.GetValue(this) as IDictionary;
                    if (dictionary != null)
                    {
                        dictionary.Clear();
                    }
                }
 
                if (!fieldType.IsPrimitive)
                {
                    field.SetValue(this, null);
                }
            }
        }

        protected void Awake()
        {
            Initialize();
        }

        protected void OnValidate()
        {
            Initialize();
        }

        protected virtual void Initialize()
        {
            
        }

        public void Log(string message) {
            ShivaDebug.MasterLog(ShivaLogLevel, message);
        }
        public void LogWarning(string message) {
            ShivaDebug.MasterLog(ShivaDebug.LogLevel.Warning, message);
        }
        public void LogError(string message) {
            ShivaDebug.MasterLog(ShivaDebug.LogLevel.Error, message);
        }
        public void LogHigher(ShivaDebug.LogLevel level, string message) {
            var lx = ShivaLogLevel;
            if ((int)level > (int)lx)
                lx = level;
            ShivaDebug.MasterLog(lx, message);
        }
    }
}