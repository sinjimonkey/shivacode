﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ShivaCode.Math
{
    [Serializable,InlineProperty]
    public struct DoubleVector
    {
        public static DoubleVector ParseGPS(string s)
        {
            if (String.IsNullOrWhiteSpace(s))
                return new DoubleVector(0, 0, 0, 0);
            var t = s.Split(',');
            var b = double.TryParse(t[0], out var lat);
            if (!b) lat = 0;
            double.TryParse(t.Length > 1 ? t[1] : "", out var lng);
            if (!b) lng = 0;
            return new DoubleVector {lat = lat, lng = lng};
        }
        
        public double x;
        public double y;
        public double z;
        public double q;
        public double lng {
            get => x;
            set => x = value;
        }
        public double lat {
            get => y;
            set => y = value;
        }

        public DoubleVector(double x, double y, double z = 0, double q = 0)
        {
            this.q = q;
            this.z = z;
            this.y = y;
            this.x = x;
        }

        public double DistanceTo(DoubleVector other)
        {
            var distance = System.Math.Pow(this.q - other.q, 2);
            distance += System.Math.Pow(this.z - other.z, 2);
            distance += System.Math.Pow(this.y - other.y, 2);
            distance += System.Math.Pow(this.x - other.x, 2);
            return System.Math.Sqrt(distance);
        }

        public override string ToString() {
            String s = $"{x}, {y}";
            if (z != 0 || q > 0) {
                s += $", {z}";
            }
            if (q > 0) {
                s += $", {q}";
            }
            return s;
        }
    }
}
