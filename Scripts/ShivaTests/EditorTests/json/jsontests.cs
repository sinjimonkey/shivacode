﻿using System.Diagnostics;
using NUnit.Framework;
//using ShivaCode.JSON;
using UnityEditor.VersionControl;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace json
{
    // ReSharper disable once InconsistentNaming
    public class jsontests
    {/*
        [Test]
        public void set_and_get()
        {
            //  -- Setup
            var t = ScriptableObject.CreateInstance<ShivaJSON>();
            t.RawString = "{\"test\" : \"value\"}";
            Debug.Log(t.RawString);
            //  -- Action
            var n = t.GetString("test", "fail");
            //  -- Assert
            Assert.AreEqual("value", n);
        }
        [Test]
        public void set_and_get_nested()
        {
            //  -- Setup
            var t = ScriptableObject.CreateInstance<ShivaJSON>();
            t.RawString = "{\"foo\" : {\"bar\" : \"baz\"}}";
            Debug.Log(t.RawString);
            //  -- Action
            var n = t.GetString("foo/bar", "fail");
            //  -- Assert
            Assert.AreEqual("baz", n);
        }
        [Test]
        public void add_key_and_retrieve()
        {
            //  -- Setup
            var t = ScriptableObject.CreateInstance<ShivaJSON>();
            //  -- Action
            t.SetString("x","Fail1");
            t.SetString("y","Win");
            t.SetString("z","Fail2");
            UnityEngine.Debug.Log(t.RawString);
            //  -- Assert
            var n = t.GetString("y", "Fail3");
            Assert.AreEqual("Win", n);
        }
        [Test]
        public void add_path_and_retrieve()
        {
            //  -- Setup
            var t = ScriptableObject.CreateInstance<ShivaJSON>();
            //  -- Action
            t.SetString("x","Fail5");
            t.SetString("y","Fail6");
            t.SetString("Foo/Bar/Baz","Win");
            t.SetString("Foo/Bar/Butz","Fail1");
            t.SetString("Foo/Bin/Baz","Fail2");
            t.SetString("Flex/In/Pink","Fail4");
            Debug.Log(t.RawString);
            //  -- Assert
            var n = t.GetString("Foo/Bar/Baz", "Fail3");
            Assert.AreEqual("Win", n);
        }
        [Test]
        // ReSharper disable once InconsistentNaming
        public void reserialize()
        {
            //  -- Setup
            var t = ScriptableObject.CreateInstance<ShivaJSON>();
            //  -- Action
            t.SetString("x","Fail5");
            t.SetString("y","Fail6");
            t.SetString("Foo/Bar/Baz","Win");
            t.SetString("Foo/Bar/Butz","Fail1");
            t.SetString("Foo/Bin/Baz","Fail2");
            t.SetString("Flex/In/Pink","Fail4");
            var u = t.RawString;
            var v = ScriptableObject.CreateInstance<ShivaJSON>();
            v.RawString = u;
            UnityEngine.Debug.Log(v.RawString);
            //  -- Assert
            var n = v.GetString("Foo/Bar/Baz", "Fail3");
            Assert.AreEqual("Win", n);
        }
    */}
}
