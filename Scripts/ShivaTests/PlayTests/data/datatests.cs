﻿using System;
using ShivaCode;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using NUnit.Framework;
using ShivaCode.DeviceWrapper;
using ShivaCode.ProcessWedges.NetworkWedges;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.TestTools;
using Assert = UnityEngine.Assertions.Assert;
using Debug = System.Diagnostics.Debug;
using ShivaCode.ProcessWedges;
using ShivaCode.ScriptableData;
using Void = ShivaCode.Void;

//using UnityEngine.ResourceManager;


namespace data
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class booltests
    {
        [UnityTest]
        public IEnumerator or_test_recognizes_one_true()
        {
            // -- SETUP
            var getter = Addressables.LoadAssetAsync<BoolOrData>("ShivaCode/Test/OrTest");

            while (!getter.IsDone)
            {
                yield return new WaitForSeconds(0.1f);
            }

            var boolOrData = getter.Result;

            foreach (var comp in boolOrData.comparators)
            {
                comp.SetFalse();
            }
            
            // -- ACTION
            var index = Math.Min(2, boolOrData.comparators.Count) - 1;
            boolOrData.comparators[index].SetTrue();

            yield return new WaitForSeconds(0.1f);

            // -- ASSERT
            
            Assert.IsTrue(boolOrData.boolData);
        }
        [UnityTest]
        public IEnumerator or_test_becomes_false_when_changed()
        {
            // -- SETUP
            var getter = Addressables.LoadAssetAsync<BoolOrData>("ShivaCode/Test/OrTest");

            while (!getter.IsDone)
            {
                yield return new WaitForSeconds(0.1f);
            }

            var boolOrData = getter.Result;

            foreach (var comp in boolOrData.comparators)
            {
                comp.SetFalse();
            }
            
            // -- ACTION
            var index = Math.Min(2, boolOrData.comparators.Count) - 1;
            boolOrData.comparators[index].SetTrue();

            yield return new WaitForSeconds(0.1f);

            boolOrData.comparators[index].SetFalse();

            // -- ASSERT
            
            Assert.IsFalse(boolOrData.boolData);
        }
        [UnityTest]
        public IEnumerator and_test_is_false_when_only_one_true()
        {
            // -- SETUP
            var getter = Addressables.LoadAssetAsync<BoolAndData>("ShivaCode/Test/AndTest");

            while (!getter.IsDone)
            {
                yield return new WaitForSeconds(0.1f);
            }

            var boolAndData = getter.Result;

            foreach (var comp in boolAndData.comparators)
            {
                comp.SetFalse();
            }
            
            // -- ACTION
            var index = Math.Min(2, boolAndData.comparators.Count) - 1;
            boolAndData.comparators[index].SetTrue();

            // -- ASSERT
            
            Assert.IsFalse(boolAndData.boolData);
        }
        [UnityTest]
        public IEnumerator and_test_is_true_when_all_true()
        {
            // -- SETUP
            var getter = Addressables.LoadAssetAsync<BoolAndData>("ShivaCode/Test/AndTest");

            while (!getter.IsDone)
            {
                yield return new WaitForSeconds(0.1f);
            }

            var boolAndData = getter.Result;

            // -- ACTION
            foreach (var comp in boolAndData.comparators)
            {
                comp.SetTrue();
            }
            
            // -- ASSERT
            
            Assert.IsTrue(boolAndData.boolData);
        }
    }

}
