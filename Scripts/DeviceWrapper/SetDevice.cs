﻿using System.Collections;
using System.Collections.Generic;
using ShivaCode;
using ShivaCode.Events;
using UnityEngine;
namespace ShivaCode.DeviceWrapper {
    public class SetDevice : BaseMonobehaviour {
        public DeviceWrapper Android;
        public DeviceWrapper iOS;
        [Space]
        public bool DoOnAwake;

        public DeviceWrapper Set() {
#if UNITY_ANDROID
            if (Android != null)
            {
                DeviceWrapper.SetDevice(Android);
            UnityEngine.Debug.Log("Device Wrapper Set:  Android");
                return Android;
            }
#elif UNITY_IOS
            if (iOS != null){
                DeviceWrapper.SetDevice(iOS);
            UnityEngine.Debug.Log("Device Wrapper Set:  iOS");
                return iOS;
            }
#endif
            UnityEngine.Debug.Log("Device Wrapper Set:  NULL");
            return null;
        }

         void Awake() {
            if (DoOnAwake)
                Set();
            base.Awake();
        }
    }
}
