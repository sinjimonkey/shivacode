﻿using System.Net;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;
using UnityEditor;

#if UNITY_IOS
using UnityEngine.iOS;
#endif


namespace ShivaCode.DeviceWrapper
{
    [CreateAssetMenu(fileName = "iOS Device", menuName = "ShivaCode/Device Wrappers/iOS")]
    // ReSharper disable once InconsistentNaming
    public class iOSWrapper : DeviceWrapper
    {
#if UNITY_IOS
        //[DllImport("__Internal")]
        //private static extern void MuteOtherSources(bool mute);
        //[DllImport("__Internal")]
        //private static extern void setAudioSessionPlayback();
        
        [DllImport ("__Internal")]
        private static extern void iOSAudio_setupAudioSession();

        public override string DeviceKey => "iOS";

        public override void MuteSources(bool mute)
        {
            //MuteOtherSources(mute);
        }

        public override void SetAudioSessionPlayback()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                iOSAudio_setupAudioSession();
            }
        }

        public override void SetNoBackup(string filePath, bool noBackup)
        {
            if (noBackup)
                Device.SetNoBackupFlag(filePath);
            else 
                Device.ResetNoBackupFlag(filePath);
        }

#endif
    }
}