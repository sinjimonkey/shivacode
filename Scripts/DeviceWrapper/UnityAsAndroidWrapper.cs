﻿using UnityEngine;

namespace ShivaCode.DeviceWrapper
{
    [CreateAssetMenu(fileName = "UnityAsAndroid", menuName = "ShivaCode/Device Wrappers/UnityAsAndroid")]
    public class UnityAsAndroidWrapper : DeviceWrapper
    {
    }
}