﻿using System;
using System.Collections;
using System.Collections.Generic;
using ShivaCode.Network;
using ShivaCode.ProcessWedges;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System.IO;
using System.Threading.Tasks;
using Debug = UnityEngine.Debug;
using ShivaCode.Debug;
using ShivaCode.ScriptableData;
using Sirenix.OdinInspector;

namespace ShivaCode.DeviceWrapper
{
    public class DeviceWrapper : BaseScriptable
    {
        [ShowInInspector]
        [SerializeField]
        private BoolData IOLockBool;
        public static string FixDirSeparators(string path)
        {
            var ret = path;
            ret = ret.Replace('/', Path.DirectorySeparatorChar);
            ret = ret.Replace('\\', Path.DirectorySeparatorChar);

            return ret;
        }

        public static string PrependDevicePath(string path)
        {
            
            var dev = DeviceWrapper.ActiveDevice.DeviceKey;
            if (!path.Contains(dev))
                path = $"{dev}/{path}";
            return path;
        }

        public static string SanitizePath(string path)
        {
            var ret = path;
            
            // Many devices don't like folders called 'movies' so we fix.
            ret = ret.Replace("movie", "videoContent");

            return FixDirSeparators(ret);
        }

        public static string PrependDataPath(string path, bool persistent = true)
        {
            path = FixDirSeparators(path);
            var pre = persistent ? Application.persistentDataPath : Application.dataPath;
            pre = FixDirSeparators(pre);
            var ret = path.Contains(pre) ? path : Path.Combine(pre, path);
            
            return FixDirSeparators(ret);
        }

        private static DeviceWrapper _activeDevice = null;

        [SerializeField, HideInInspector]

        public static DeviceWrapper ActiveDevice
        {
            get => _activeDevice;
        }

        private string deviceKey = "Default";
        // ReSharper disable once ConvertToAutoProperty
        // ReSharper disable once MemberCanBePrivate.Global
        public virtual string DeviceKey => deviceKey;
        private static Queue<Action<DeviceWrapper>> pending = new Queue<Action<DeviceWrapper>>();

        public static void DoWithDevice(Action<DeviceWrapper> func)
        {
            if (_activeDevice != null)
            {
                func(_activeDevice);
            }
            else
            {
                ShivaDebug.MasterLog(ShivaDebug.LogLevel.Debug, "Loading Device");    
                pending.Enqueue(func);
                if (pending.Count <= 1)
                {
                    IEnumerator coroutine = LoadDevice();
                    CoroutineStarter.AddCoroutine(coroutine);
                }
            }
        }

        private static IEnumerator LoadDevice()
        {
            string s = "UnityAsAndroid";
            #if UNITY_ANDROID
            s = "AndroidWrapper";
            #elif UNITY_IOS
            s = "iOSWrapper";
            #endif
            ShivaDebug.MasterLog(ShivaDebug.LogLevel.Debug, "Getting Device Profile:  "+s);
            var t = Addressables.LoadAssetAsync<DeviceWrapper>("ShivaCode/DeviceWrapper/"+s);
            while (!t.IsDone)
            {
                yield return new WaitForSeconds(0.1f);
            }
            ShivaDebug.MasterLog(ShivaDebug.LogLevel.Debug, "Device Profile Selected:  " + s);

            SetDevice(t.Result);
        }

        public static void SetDevice(DeviceWrapper device) {
            if (device == null)
                return;
            ShivaDebug.MasterLog(ShivaDebug.LogLevel.Warning, $"DeviceWrapper ({device.name})");
            _activeDevice = device;
            DoPending();
        }

        public static void DoPending() {
            while (pending.Count > 0) {
                pending.Dequeue().Invoke(_activeDevice);
            }
        }


        public enum PermissionType
        {
            Camera,
            GPS,
            PhotoLibrary
        }

        public virtual bool CheckPermission(PermissionType permission)
        {
            return true;
        }

        public virtual void RequestPermission(PermissionType permission)
        {
        }

        public virtual void CreateDirectories(string path, bool noBackup = true)
        {
            var filePath = Application.persistentDataPath;
            var pathBits = path.Split('/');

            if (pathBits.Length <= 1) return;
            for (var x = 0; x < pathBits.Length - 1; x++) // length -1 because we don't want to do the last one.
            {
                filePath = Path.Combine(filePath, pathBits[x]);
                filePath = SanitizePath(filePath);
                if (!Directory.Exists(filePath))
                    Directory.CreateDirectory(filePath);
                SetNoBackup(filePath, noBackup);
            }
        }

        public virtual void SetNoBackup(string filePath, bool noBackup)
        {
            // only applies to iOS
        }

        public virtual void CreateFile(string path, bool noBackup = true, bool persistent = true)
        {
            path = PrependDataPath(path, persistent);
            CreateDirectories(path, persistent);
            if (!File.Exists(path))
                File.Create(path);
            SetNoBackup(path, noBackup);
        }

        public virtual void WriteFile(string path, string text, bool noBackup = true, bool persistent = true)
        {
            path = PrependDataPath(path, persistent);
            CreateFile(path, noBackup, persistent: persistent);
            UnityEngine.Debug.Log(path);
            IEnumerator Act()
            {
                while (IOLockBool.isTrue)
                    yield return new WaitForSeconds(0.1f);
                
                //UnityEngine.Debug.Log("IOLock Released");
                IOLockBool.SetTrue();
                Task v = null;
                StreamWriter file = null;
                bool catchError = false;
                try
                {
                    file = new StreamWriter(path);
                    v = file.WriteAsync(text);
                }
                catch
                {
                    catchError = true;
                }

                if (catchError)
                {
                    IOLockBool.SetFalse();
                    yield return new WaitForSeconds(0.5f);
                    yield return Act();
                    yield break;
                }

                while (!v.IsCompleted)
                    yield return new WaitForSeconds(0.1f);
                file.Close();
                IOLockBool.SetFalse();
                //UnityEngine.Debug.Log("Write Finished");
            }
            CoroutineStarter.AddCoroutine(Act());
        }

        public virtual void ReadFile(string path, Action<string> callback, Action<string> errorCallback, bool persistent = true)
        {
            path = PrependDataPath(path, persistent);
            UnityEngine.Debug.Log(path);
            if (!File.Exists(path))
                callback("");
            else
            {
                var s = File.ReadAllText(path);
                UnityEngine.Debug.Log("Device File Read:  " + path + "/n" + s);
                callback(s);
            }

        }

        public virtual bool CheckFileExists(string path, bool persistent = true)
        {
            path = PrependDataPath(path, persistent);
            path = SanitizePath(path);
            return (File.Exists(path));
        }

        public virtual void MuteSources(bool mute)
        {
            
        }
        public virtual void SetAudioSessionPlayback()
        {
            
        }

        public virtual void VuphoriaScreenFix() {

        }
    }
}