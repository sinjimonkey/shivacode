﻿using System;
using ShivaCode.ProcessWedges;
using UnityEngine;
using UnityEngine.Android;
#if UNITY_ANDROID

#endif

namespace ShivaCode.DeviceWrapper
{
    [CreateAssetMenu(fileName = "Android Device", menuName = "ShivaCode/Device Wrappers/Android")]
    public class AndroidWrapper : global::ShivaCode.DeviceWrapper.DeviceWrapper
    {
        public override string DeviceKey => "Android";
#if UNITY_ANDROID

        private string GetPermissionString(PermissionType permission)
        {
            switch (permission)
            {
                case PermissionType.Camera:
                    return Permission.Camera;
                case PermissionType.GPS:
                    return Permission.FineLocation;
                case PermissionType.PhotoLibrary:
                    return Permission.ExternalStorageWrite;
                //break;
            }

            return "";
        }

        public override bool CheckPermission(PermissionType permission)
        {
            var permissionString = GetPermissionString(permission);
            if (string.IsNullOrWhiteSpace(permissionString))
                return true;
            var v = permissionString;
            var b = Permission.HasUserAuthorizedPermission(permissionString);
            Log($"Permission Check ({v}) - {b}");
            return b;
        }

        public override void RequestPermission(PermissionType permission)
        {
            if (!CheckPermission(permission))
            {
                var permissionString = GetPermissionString(permission);
                Permission.RequestUserPermission(permissionString);
                Log($"Requesting Permission:   ({permissionString})");
            }
        }

#endif
    }
}