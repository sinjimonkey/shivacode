﻿using ShivaCode.Events;

namespace ShivaCode.Behaviours
{
    public class DontDestroyOnLoad : BaseMonobehaviour
    {
        new void Awake()
        {
            DontDestroyOnLoad(this);
            base.Awake();
        }
    }
}
