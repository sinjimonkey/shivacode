﻿using System;
using System.Collections;
using System.Collections.Generic;
using ShivaCode;
using ShivaCode.DeviceWrapper;
using ShivaCode.ProcessWedges;
using ShivaCode.ScriptableData;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using Debug = System.Diagnostics.Debug;

public class Sceneloader : BaseMonobehaviour
{
    
    [SerializeField] private string sceneName = "";
    [SerializeField] private bool preload = false;
    [SerializeField] private bool checkPermissions = false;

    [SerializeField, ShowIf("checkPermissions")]
    private List<DeviceWrapper.PermissionType> permissions = new List<DeviceWrapper.PermissionType>();

    [SerializeField] private List<GameObject> disableChildrenOnLoad = new List<GameObject>();
    [SerializeField] private List<GameObject> enableChildrenOnLoad = new List<GameObject>();
    [SerializeField] private BoolData asyncBlocked;
    private AsyncOperation _aSync = null; 
    
    
    void Start()
    {
        if (preload)
            CoroutineStarter.DelayFunction(Preload,2f);
    }

    void Update()
    {
        if (_aSync == null)
            return;
        var f = _aSync.progress;
    }

    public void Preload()
    {
        if (asyncBlocked != null)
            asyncBlocked.SetTrue();
        _aSync = SceneManager.LoadSceneAsync(sceneName);
        _aSync.allowSceneActivation = false;
    }

    private Dictionary<DeviceWrapper.PermissionType, bool> tries = new Dictionary<DeviceWrapper.PermissionType, bool>();
    private Dictionary<DeviceWrapper.PermissionType, bool> validations = new Dictionary<DeviceWrapper.PermissionType, bool>();

    public void Load() {
        Load(true);
    }

    public void Load(bool checkPerms) {
        Log("Trigger Load:  " + sceneName);
        if (checkPerms && checkPermissions) {
            DeviceWrapper.DoWithDevice(x=>ValidatePermissions(x));
            return;
        }
        foreach (var child in disableChildrenOnLoad)
        { 
            child.SetActive(false);   
        }
        foreach (var child in enableChildrenOnLoad)
        {
            if (child == null) continue;
            child.SetActive(true);   
        }
        if (_aSync == null)
            _aSync = SceneManager.LoadSceneAsync(sceneName);
        CoroutineStarter.DelayFunction(() => {
            if (_aSync != null)
                _aSync.allowSceneActivation = true;
            if (asyncBlocked != null)
                asyncBlocked.SetFalse();
        }, 0.1f);

    }

    private void ValidatePermissions(DeviceWrapper device) {
        Log("Sceneloader.ValidatePermissions");
        bool abort = false;
        foreach (var perm in permissions) {
            if (!device.CheckPermission(perm)) {
                abort = true;
                if (!tries.ContainsKey(perm)) {
                    device.RequestPermission(perm);
                    tries.Add(perm, true);
                } else {
                    // error
                }
            }
        }
        if (abort)
            return;
        Load(false);
    }
}
