﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace ShivaCode {
    [RequireComponent(typeof(CanvasScaler))]
    public class ShivaCanvas : BaseMonobehaviour {
        public static void SetBounds(RectTransform rt, float left, float right, float bottom, float top) {
            rt.offsetMin = new Vector2(left, bottom);
            rt.offsetMax = new Vector2(right, top);
        }

        public static void getBounds(RectTransform rt, out float left, out float right, out float bottom, out float top) {
            left = rt.offsetMin.x;
            bottom = rt.offsetMin.y;
            right = rt.offsetMax.x;
            top = rt.offsetMax.y;
        }

        private Vector2 Scalar => GetComponent<CanvasScaler>().referenceResolution;
        private Vector2 WorldCornersDiff(RectTransform rt) {
            Vector3[] corners = new Vector3[4];
            rt.GetWorldCorners(corners);
            var width = corners[0].x - corners[3].x;
            var height = corners[0].y - corners[1].y;
            return new Vector2(width, height);
        }
        public Vector2 RectDiff(RectTransform rt) {
            // we get the difference in size in world coordinates between the rt and the canvas.
            var canvasDiff = WorldCornersDiff(GetComponent<RectTransform>());
            var rtDiff = WorldCornersDiff(rt);
            var netDiff = rtDiff / canvasDiff;
            // then we multiply by the reference resolution of the Canvas.
            var scalar = Scalar;
            return scalar * netDiff;
        }
    }

}
