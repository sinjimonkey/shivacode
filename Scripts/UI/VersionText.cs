﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionText : MonoBehaviour
{
    [SerializeField] private Text text = null;
    private void OnValidate() {
        if (text != null) {
            string s = Application.version;
            if (Application.version != null)
                text.text = $"Augle {s}";
            else
                text.text = "????";
        }
    }
}
