﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonMultiColor : UnityEngine.UI.Button {

    protected override void DoStateTransition(SelectionState state, bool instant) {
        Color color;
        switch (state) {
            case Selectable.SelectionState.Normal:
                color = this.colors.normalColor;
                break;
            case Selectable.SelectionState.Highlighted:
                color = this.colors.highlightedColor;
                break;
            case Selectable.SelectionState.Pressed:
                color = this.colors.pressedColor;
                break;
            case Selectable.SelectionState.Disabled:
                color = this.colors.disabledColor;
                break;
            default:
                color = this.colors.normalColor;
                break;
        }
        if (base.gameObject.activeInHierarchy) {
            switch (this.transition) {
                case Selectable.Transition.ColorTint:
                    ColorTween(color * this.colors.colorMultiplier, instant);
                    break;
            }
        }
    }

    private void ColorTween(Color targetColor, bool instant) {
        if (this.targetGraphic == null) {
            this.targetGraphic = this.image;
        }
        var graphics = GetComponentsInChildren<Graphic>();
        try {
            base.image.CrossFadeColor(targetColor, (!instant) ? this.colors.fadeDuration : 0f, true, true);
        } catch  {
        }
        foreach (var g in graphics) {
            g.CrossFadeColor(targetColor, (!instant) ? this.colors.fadeDuration : 0f, true, true);
        }

    }


    public void SetColor(Color targetColor)
    {
        ColorTween(targetColor, true);
    }
    public void MakeBlack()
    {
        ColorTween(Color.black, true);
    }
}