﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using System.Collections;
using ShivaCode;
using ShivaCode.Interfaces;

[RequireComponent(typeof(Image))]
public class ImageSetter : BaseMonobehaviour, IReturnHandler<Sprite>
{
    public GameObject hideNull = null;
    [FoldoutGroup("Options")]
    public bool constrainToParent = true;
    [FoldoutGroup("Options"), HideIf("constrainToParent")]
    public bool fixedWidth = true;
    [FoldoutGroup("Options"), HideIf("constrainToParent")]
    public bool fixedHeight = false;
    [FoldoutGroup("Options"), HideIf("constrainToParent")]
    public bool square = false;
    [FoldoutGroup("Options"), HideIf("constrainToParent")]
    public bool minDim = false;
    [FoldoutGroup("Options"), HideIf("constrainToParent")]
    public bool passSubSetter = false;
    [FoldoutGroup("Options"), HideIf("constrainToParent")]
    [ShowIf("passSubSetter")]
    [Indent]
    public ImageSetter subSetter = null;
    [FoldoutGroup("Options")]
    public bool overrideOriginalSize = false;
    [FoldoutGroup("Options")]
    public bool useDeltaSize = false;
    [FoldoutGroup("Options")]
    [ShowIf("overrideOriginalSize")]
    [Indent]
    public Vector2 originalSize = Vector2.zero;

    [FoldoutGroup("Options"), HideIf("constrainToParent")]
    public bool fixParentLayoutElement = false;

    [FoldoutGroup("Options"), HideIf("constrainToParent")] 
    [ShowIf("fixParentLayoutElement")]
    [Indent]
    public LayoutElement parentLayoutElement;

    [Space]
    [DrawWithUnity]
    public UnityEvent contentChanged;

    private bool doUpdate = true;
    private Sprite sprite;
    private Color color;

    public void setSprite(Sprite sprite, Color color) {
        if (passSubSetter && subSetter != null) {
            subSetter.setSprite(sprite, color);
            return;
        }
        this.sprite = sprite;
        this.color = color;
        setImage();
        doUpdate = true;
    }

    public void setSprite(Sprite sprite) {
        setSprite(sprite, Color.white);
    }

    private void setImage() {
        //Debug.LogWarning($"Setting Image:  {sprite.name} -> {this.name}");
        Image image = GetComponent<Image>();
        if (image == null) return;
        image.sprite = sprite;
        image.color = color;
        doUpdate = true;
    }
    [Button]
    public void fixSize() {
        Debug.LogWarning("Fix Size:  " + name);
        if (sprite == null)
            return;
        RectTransform rt = GetComponent<Image>().rectTransform;
        /*Vector3[] corners = new Vector3[4];
        rt.GetWorldCorners(corners);
        Debug.Log(corners[0] + " " + corners[1] + " " + corners[2]+" "+corners[3]);
        */


        if (overrideOriginalSize) {
            rt.sizeDelta = originalSize;
        }

        Vector2 rectDiff = GetParentCanvas().RectDiff(rt);
        var height = rectDiff.y;
        var width = rectDiff.x;
        
        if (useDeltaSize) {
            height = rt.sizeDelta.y;
            width = rt.sizeDelta.x;
        }

        Vector3 spriteSize = sprite.bounds.size;
        float imgWidth = spriteSize.x;
        float imgHeight = spriteSize.y;

        if (constrainToParent) {
            var parentForm = transform.parent.GetComponent<RectTransform>();
            rt.sizeDelta = parentForm.sizeDelta;
            fixedHeight = false;
            fixedWidth = false;
            square = false;
            minDim = false;
            if ((imgWidth / rt.sizeDelta.x) > (imgHeight / rt.sizeDelta.y))
                fixedHeight = true;
            else
                fixedWidth = true;
        }

        bool doHeight = fixedHeight;
        bool doWidth = fixedWidth;

        if (minDim) {
            doHeight = false;
            doWidth = false;
            if (imgHeight <= imgWidth)
                doHeight = true;
            else
                doWidth = true;
        } else if (square) {
            doHeight = false;
            doWidth = false;
            if (imgHeight >= imgWidth)
                doHeight = true;
            else
                doWidth = true;
        }

        if (doWidth) {
            if (doHeight)
                return;

            height = (width * imgHeight) / imgWidth;
            Debug.LogFormat("{0} / {1} * {2}",width, imgWidth, imgHeight);
            Debug.Log("Height:  " + height);
            rt.sizeDelta = new Vector2(rt.sizeDelta.x, height);
        } else if (doHeight) {
            width = (height * imgWidth) / imgHeight;
            Debug.LogFormat("{0} / {1} * {2}", height, imgHeight, imgWidth);
            Debug.Log("Width:  " + width);
            rt.sizeDelta = new Vector2(width, rt.sizeDelta.y);

        }

        if (fixParentLayoutElement && parentLayoutElement != null)
        {
            parentLayoutElement.minHeight = height;
            parentLayoutElement.minWidth = width;
            RebuildRects();
        }

        //change the content size fitter.
        /*MagicContentFitter mcf = GetComponent<MagicContentFitter>();
        if (mcf != null) {
            if (mcf.overrideSize)
                mcf.forcedSize = height;
            else 
                mcf.minSize = height;
            mcf.updateContent();
        }*/
        
    }

    public void setActive(bool b) {
        gameObject.SetActive(b);
        doUpdate = true;
    }

    private void OnEnable() {
        doUpdate = true;
    }


    private void Update() {
        if (doUpdate) {
            fixSize();
            contentChanged.Invoke();
            doUpdate = false;
        }
    }

    private void Awake() {
        /*MagicContentFitter mcf = GetComponent<MagicContentFitter>();
        if (mcf != null) {
            contentChanged.AddListener(mcf.updateContent);
        }*/
        
    }

    public void Handle(Sprite result)
    {
        if (hideNull != null)
           hideNull.SetActive(result != null); 
        setSprite(result);
    }
}
