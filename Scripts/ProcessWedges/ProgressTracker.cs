﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ShivaCode.ProcessWedges
{

    public class ProgressTracker
    {
        private static ProgressTracker _bundleProgress = null;

        public static ProgressTracker BundleProgress
        {
            get
            {
             if (_bundleProgress == null)
                 _bundleProgress = new ProgressTracker();
             return _bundleProgress;
            }
        }

        public List<ProgressBit> List = new List<ProgressBit>();
        
        public class ProgressBit
        {
            public System.Func<float> Progress;
            public int Priority;
            public string Category;

            public ProgressBit(System.Func<float> progress, int priority, string category)
            {
                Progress = progress;
                Priority = priority;
                Category = category;
            }

        }

        public void Purge()
        {
            if (List.Count == 0)
                return;
            for (int n = List.Count-1; n >= 0; n--)
            {
                if (List[n].Progress() >= 1)
                    List.RemoveAt(n);
            }
        }

        public void AddBit(ProgressBit bit)
        {
            List.Add(bit);
            Purge();
        }

        public float GetProgress(out string label)
        {
            label = "";
            float lowest = 1f;
            int priority = Int32.MinValue;

            foreach (var prog in List)
            {
                if (prog.Priority < priority)
                    continue;
                
                var currentItemProgress = prog.Progress();
                
                if (prog.Priority > priority)
                {
                    priority = prog.Priority;
                    lowest = currentItemProgress;
                    label = prog.Category;
                }
                else if (currentItemProgress < lowest)
                {
                    lowest = currentItemProgress;
                    label = prog.Category;
                }
            }

            return lowest;
        }
    }
}