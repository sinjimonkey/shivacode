﻿using System;
using System.Collections;
using System.Collections.Generic;
using ShivaCode.Debug;
using UnityEngine;
using UnityEngine.AddressableAssets;
namespace ShivaCode.ProcessWedges
{
    
    public class CoroutineStarter : BaseMonobehaviour
    {
        private static CoroutineStarter _instance = null;
        private static readonly List<IEnumerator> Pending = new List<IEnumerator>();

        private static CoroutineStarter Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;
                var t = Addressables.LoadAssetAsync<GameObject>("ShivaCode/CoroutineStarter");
                t.Completed += (mono) =>
                {

                    CoroutineStarter inst = mono.Result.GetComponent<CoroutineStarter>();

                    if (inst == null) return;
                    
                    inst = Instantiate(inst);
                    _instance = inst;

                    for (int i = Pending.Count - 1; i >= 0; i--)
                    {
                        _instance.StartCoroutine(Pending[i]);
                        Pending.RemoveAt(i);
                    }

                    DontDestroyOnLoad(_instance);
                };

                Pending.Clear();
                
                return null;
            }
        }

        public static void AddCoroutine(IEnumerator coroutine)
        {
            if (Instance == null)
            {
                ShivaDebug.MasterLog(ShivaDebug.LogLevel.Verbose, "Coroutine added:  "+coroutine);
                Pending.Add(coroutine);
            }
            else
                Instance.StartCoroutine(coroutine);
        }

        public void Awake()
        {
            if (_instance != null && _instance != this)
                Destroy(_instance);
            _instance = this;
            DontDestroyOnLoad(this);
        }

        public static void DelayFunction(Action action, float time)
        {
            AddCoroutine(DelayedFunction(action, time));
        }

        private static IEnumerator DelayedFunction(Action action, float time)
        {
            yield return new WaitForSeconds(time);
            action.Invoke();
        }
    }
}