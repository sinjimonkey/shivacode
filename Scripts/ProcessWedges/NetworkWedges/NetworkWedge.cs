﻿using System;

namespace ShivaCode.ProcessWedges.NetworkWedges
{
    public abstract class NetworkWedge<T> : ProcessWedge<T>
    {
        protected Action Request = () => { };

        
        
        public override void Start()
        {
            Request();
        }
    }
}