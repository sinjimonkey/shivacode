﻿using System;
using System.Collections.Generic;
using System.Linq;
using ShivaCode.Events;
using ShivaCode.Interfaces;
using ShivaCode.Network;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ShivaCode.ProcessWedges.NetworkWedges
{
    [CreateAssetMenu(fileName = "TitleDataWedge", menuName = "ShivaCode/Network/TitleDataWedge")]
    
    public class TitleDataWedge : NetworkWedge<Dictionary<string,string>>
    {
        [SerializeField, PropertyOrder(-3)] public INetworkGetTitleData overrideTitleData;
        [SerializeField,PropertyOrder(-2)]
        public Dictionary<string, IReturnHandler<string>> keyHandlers = new Dictionary<string, IReturnHandler<string>>();
        public override void Setup(GUIDEventObject guidEventObject)
        {
            base.Setup(guidEventObject);
            var keys = keyHandlers.Keys.ToList();
            if (overrideTitleData != null)
                overrideTitleData.GetNetworkTitleKeys(keys, CallSuccess, CallFail);
            else
                ShivaNetwork.GetTitleData(keys, CallSuccess, CallFail);
        }
        
        private void CallFail(string mess)
        {
            UnityEngine.Debug.Log("TitleData Fail");
            FinishFail(mess);
        }

        private void CallSuccess(Dictionary<string,string> result)
        {
            UnityEngine.Debug.Log("TitleData Success");
            foreach (var dat in result.Keys)
            {
                UnityEngine.Debug.Log($"{dat}:  {result[dat]}");
                if (keyHandlers.ContainsKey(dat))
                    keyHandlers[dat]?.Handle(result[dat]);
            }
            FinishSuccess(result);
        }


        public override void Update()
        {
        }

        public override void FinishSuccess()
        {
            UnityEngine.Debug.Log("FinishSuccess");
            base.FinishSuccess();
        }
        
    }
}