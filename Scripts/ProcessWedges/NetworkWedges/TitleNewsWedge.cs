﻿using System.Collections.Generic;
using ShivaCode.Events;
using ShivaCode.Network;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ShivaCode.ProcessWedges.NetworkWedges
{
    [CreateAssetMenu(fileName = "TitleNewsWedge", menuName = "ShivaCode/Network/TitleNewsWedge")]
    public class TitleNewsWedge : NetworkWedge<Dictionary<string,string>>
    {
        
        [PropertyOrder(-1)] public INetworkGetMOTD overrideMOTD;
        
        public override void Setup(GUIDEventObject guidEventObject)
        {
            Log(("Title News Setup"));
            base.Setup(guidEventObject);
            Start();
        }

        public override void Start()
        {
            if (overrideMOTD != null)
                overrideMOTD.GetNetworkMOTD(FinishSuccess, FinishFail);
            else
               ShivaNetwork.GetMOTD(FinishSuccess, FinishFail);
        }

        public override void Update()
        {
            
        }

        public override void FinishSuccess()
        {
            Log("Title News FinishSuccess");
            base.FinishSuccess();
        }
        
    }
}