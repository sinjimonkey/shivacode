﻿using ShivaCode.Events;
using ShivaCode.Interfaces;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ShivaCode.ProcessWedges
{
    public abstract class ProcessWedge<T> : BaseScriptable, IProcessWedge
    {
        [SerializeField] public VoidEvent startListeners = null;
        [SerializeField] public VoidEvent successListeners = null;
        [SerializeField, HideIf("IsVoid")] public BaseGameEvent<T> typeListeners = null;
        [SerializeField] public VoidEvent failureListeners = null;
        [Space] public IReturnHandler<T> returnHandler = null;
        public IReturnHandler<string> errorHandler = null;

        protected GUIDEventObject GUID;
        
        private bool IsVoid()
        {
            return (typeof(T) == typeof(Void));

        }

        public virtual void Setup(GUIDEventObject guidEventObject = default)
        {
            baseDone = false;
            GUID = guidEventObject;
        }

        public virtual void Start()
        {
            baseDone = false;
            if (startListeners != null) startListeners.Raise(new Void());
        }

        public virtual void Update()
        {
        }

        public virtual void FinishSuccess()
        {
            if (successListeners != null) successListeners.Raise(new Void());
            baseDone = true;
        }

        public virtual void FinishSuccess(T obj)
        {
            returnHandler?.Handle(obj);
            if (typeListeners != null) typeListeners.Handle(obj);
            GUIDEvent.GUIDWrap<T>(GUID, obj1 => { FinishSuccess(); }, true).Invoke(obj);
        }

        public virtual void FinishFail()
        {
            if (failureListeners != null) failureListeners.Raise(new Void());
        }

        protected void FinishFail(string message)
        {
            errorHandler?.Handle(message);
            GUIDEvent.GUIDWrap<string>(GUID, obj1 => { FinishFail(); }).Invoke(message);
        }

    }
}