﻿using System;
using ShivaCode.Events;
using UnityEditor;

namespace ShivaCode.ProcessWedges
{
    public class DynamicWedge : IProcessWedge
    {
        private readonly Action<GUIDEventObject> _setup;
        private readonly Action _start;
        private readonly Action _update;
        private readonly Action _finishSuccess;
        private readonly Action _finishFail;
        
        public DynamicWedge(Action<GUIDEventObject> setup = null, Action start = null, Action update = null, Action finishSuccess = null, Action finishFail = null)
        {
            _setup = setup ?? ((GUID) => { });
            _start = start ?? (() => { });
            _update = update ?? (() => { });
            _finishSuccess = finishSuccess ?? (() => { });
            _finishFail = finishFail ?? (() => { });
        }

        public void Setup(GUIDEventObject guidEventObject = default) => _setup(guidEventObject);
        public void Start() => _start();
        public void Update() => _update();
        public void FinishSuccess() => _finishSuccess();
        public void FinishFail() => _finishFail();
    }
}