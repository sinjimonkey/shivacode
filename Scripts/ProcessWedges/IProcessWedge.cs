﻿using System;
using ShivaCode.Events;

namespace ShivaCode.ProcessWedges
{
    public interface IProcessWedge
    {
        void Setup(GUIDEventObject guidEventObject);
        void Start();
        void Update();
        void FinishSuccess();
        void FinishFail();
    }
}