﻿using System.IO;
using Sirenix.Serialization;
using UnityEditor;
using UnityEngine;
using UnityEditor.Build.Reporting;
using UnityEditor.AddressableAssets.Settings;


// Output the build size or a failure depending on BuildPlayer.


public class ShivaBuild : MonoBehaviour
#if UNITY_ANDROID
, UnityEditor.Android.IPostGenerateGradleAndroidProject
#endif

{
    [MenuItem("Build/Increment Build Numbers")]
    public static void IncrementNumbers(){

        if (!int.TryParse(PlayerSettings.iOS.buildNumber, out var x))
            x = 0;
        x = System.Math.Max(x,PlayerSettings.Android.bundleVersionCode);
        x++;
        Debug.Log($"New Build Number:  {x}");
        PlayerSettings.Android.bundleVersionCode = x;
        PlayerSettings.iOS.buildNumber = x.ToString();
    }
#if UNITY_ANDROID
    
    public int callbackOrder {
        get {
            return 999;
        }
    }
 
    void UnityEditor.Android.IPostGenerateGradleAndroidProject.OnPostGenerateGradleAndroidProject(string path) {
        Debug.Log("Bulid path : " + path);
        string gradlePropertiesFile = path + "/gradle.properties";
        
        if (File.Exists(gradlePropertiesFile)) {
            File.Delete(gradlePropertiesFile);
        }
        StreamWriter writer = File.CreateText(gradlePropertiesFile);
        writer.WriteLine("org.gradle.jvmargs=-Xmx4096M");
        writer.WriteLine("android.useAndroidX=true");
        writer.WriteLine("android.enableJetifier=true");
        writer.Flush();
        writer.Close();
 
    }
    
    public static void PreBuild(){
        
        Ludiq.AotPreBuilder.PreCloudBuild();
        
        
        Debug.Log("AOT Instance Test:" + AOTGenerationConfig.Instance);
        Debug.Log("AOT Instance Test:" + (AOTGenerationConfig.Instance == null));

        Sirenix.Serialization.AOTGenerationConfig.Instance.ScanProject();
        Sirenix.Serialization.AOTGenerationConfig.Instance.GenerateDLL(
            Application.dataPath+"Plugins/Sirenix/Assemblies/AOT/"
        );
        
        
        Debug.Log("Sirenix finished");
        
        AddressableAssetSettings.CleanPlayerContent();
        AddressableAssetSettings.BuildPlayerContent();
        
    }

    public static void PostBuild (string path)
    {
        
    }
    
    [MenuItem("Build/Build Android")]
    public static void MyBuild() {
        PreBuild();
        
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Prism/Scenes/Landing.unity", "Assets/Prism/Scenes/Menu.unity", "Assets/Prism/Scenes/ARCAM.unity" };
        buildPlayerOptions.locationPathName = $"Builds/Android/Shiva/{Application.productName} {Application.version} ({PlayerSettings.Android.bundleVersionCode})" ;
        buildPlayerOptions.target = BuildTarget.Android;
        buildPlayerOptions.options = BuildOptions.None;

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded) {
            BuildSucceeded(summary);
        }

        if (summary.result == BuildResult.Failed) {
            BuildFailed(summary);
        }
        
        PostBuild(summary.outputPath);
    }

    public static void BuildSucceeded(BuildSummary summary) {
        Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
    }

    public static void BuildFailed(BuildSummary summary) {
        Debug.LogError("Build failed");
    }
#endif
#if UNITY_IOS

    public static void PreBuild()
    {
        Ludiq.AotPreBuilder.PreCloudBuild();

        var AOT = Sirenix.Serialization.AOTGenerationConfig.Instance;
        
        AOT.ScanProject();
        AOT.GenerateDLL(
            Application.dataPath+"Plugins/Sirenix/Assemblies/AOT/"
            );
        
        Debug.Log("Sirenix finished");
        AddressableAssetSettings.CleanPlayerContent();
        AddressableAssetSettings.BuildPlayerContent();
        
        PlayerSettings.muteOtherAudioSources = true;
        PlayerSettings.preserveFramebufferAlpha = true;

    }

    public static void PostBuild (string path)
    {
        
    }
    [MenuItem("Build/Build iOS")]
    public static void MyBuild() {
        
        PreBuild();
        
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Prism/Scenes/Landing.unity", "Assets/Prism/Scenes/Menu.unity", "Assets/Prism/Scenes/ARCAM.unity" };
        buildPlayerOptions.locationPathName = $"Builds/iOS/Shiva/{Application.productName} {Application.version} ({PlayerSettings.Android.bundleVersionCode})" ;
        buildPlayerOptions.target = BuildTarget.iOS;
        buildPlayerOptions.options = BuildOptions.None;

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded) {
            BuildSucceeded(summary);
        }

        if (summary.result == BuildResult.Failed) {
            BuildFailed(summary);
        }
        
        PostBuild(summary.outputPath);
    }

    public static void BuildSucceeded(BuildSummary summary) {
        Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
    }

    public static void BuildFailed(BuildSummary summary) {
        Debug.LogError("Build failed");
    }
#endif
}