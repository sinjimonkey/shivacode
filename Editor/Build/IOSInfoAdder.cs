﻿
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

public class IOSInfoAdder
{
    [PostProcessBuild]
    public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuildProject)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            //get plist
            string plistPath = pathToBuildProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));
            
            //get root
            PlistElementDict rootDict = plist.root;
            
            //change the values
            var buildKey = "Privacy - Photo Library Additions Usage Description";
            rootDict.SetString(buildKey, "Permission required to save photos taken in ARCAM mode");
            buildKey = "UIRequiresFullScreen";
            rootDict.SetBoolean(buildKey, true);
            buildKey = "NSPhotoLibraryUsageDescription";
            rootDict.SetString(buildKey, "Sharing requires library access.");
            buildKey = "NSPhotoLibraryAddUsageDescription";
            rootDict.SetString(buildKey, "Permission required to save photos taken in ARCAM mode");
            buildKey = "Privacy - Photo Library Usage Description";
            rootDict.SetString(buildKey, "Permission required to save photos taken in ARCAM mode");
            
            //write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }   
    }
}
