//var GetUserDataRequest = PlayFabServerModels.GetUserDataRequest;
//   Allocate data to be later handled by the server. 
handlers.assignData = function (args, _context) {
    var request = {
        PlayFabId: currentPlayerId,
        "Keys": ["UpdateKey"]
    };
    var values = JSON.parse("[]");
    var playerData = server.GetUserInternalData(request);
    if (playerData.Data.hasOwnProperty("UpdateKey")) {
        var data = playerData.Data["UpdateKey"];
        log.info(String(data));
        values = JSON.parse(data.Value);
    }
    log.info(String(values));
    if (values.length >= 50) {
        values = values.slice(1, values.length - 1);
    }
    values.push(args.message);
    log.info(String(values));
    var dataPayload = {};
    dataPayload["UpdateKey"] = JSON.stringify(values);
    server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataPayload
    });
    /*let playerDataResult =*/ server.UpdatePlayerStatistics({
        PlayFabId: currentPlayerId,
        Statistics: [{ StatisticName: "UpdateKey", Value: 1 }]
    });
};
// below this line is legacy
handlers.campaignVote = function (args, _context) {
    log.info("campaignVisit");
    log.info(args);
    var window = null;
    if (args && args.window && args.window.campaign && args.window.window) {
        window = args.window;
    }
    else {
        return {
            resultType: "error",
            messageValue: "Invalid Args"
        };
    }
    var message = {};
    message["window"] = window;
    var dataPayload = {};
    dataPayload["Vote" + window.campaign] = window.window;
    server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataPayload
    });
    /*let playerStatResult =*/ server.UpdatePlayerStatistics({
        PlayFabId: currentPlayerId,
        Statistics: [{ StatisticName: "Vote" + window.campaign, Value: 1 }]
    });
    return { resultType: "vote_return", messageValue: String(message) };
};
handlers.campaignVisit = function (args, _context) {
    log.info("campaignVisit");
    log.info(args);
    var window = null;
    if (args && args.window && args.window.campaign && args.window.window) {
        window = args.window;
    }
    else {
        return {
            resultType: "error",
            messageValue: "Invalid Args"
        };
    }
    var message = {};
    message["window"] = window;
    var loc = "visits_" + window.campaign;
    log.info(String(loc));
    var request = { "PlayFabId": currentPlayerId, "Keys": [loc] };
    var playerData = server.GetUserInternalData(request);
    var campaignData = {};
    if (playerData.Data.hasOwnProperty(loc)) {
        campaignData = JSON.parse(String(playerData.Data[loc]));
    }
    log.info(("campaignData" + JSON.stringify(campaignData)));
    if (!campaignData.hasOwnProperty(window.window)) {
        campaignData[(window.window)] = true;
        var playerPayload = {};
        playerPayload[loc] = JSON.stringify(campaignData);
        server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: playerPayload
        });
    }
    return { resultType: "visit_return", messageValue: String(message) };
};
